import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {Location} from '@angular/common';
import { JsondataService } from "../jsondata.service";
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../rivette.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-waiver-sign',
  templateUrl: './waiver-sign.component.html',
  styleUrls: ['./waiver-sign.component.scss'],
})
export class WaiverSignComponent implements OnInit {
	custPhone;
	custId;
	clientId;
	formGroup;
	waiverText;

	constructor(
		private _location: Location,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private jsondata:  JsondataService,
		private apiService:  RivetteService,
		public toastController: ToastController,
		private http: HttpClient
	) {
		this.custId = localStorage.getItem('custId');
		this.clientId = localStorage.getItem('client_id');
		this.custPhone = localStorage.getItem('custPhone');
	}

	ngOnInit() {
		let client_id = localStorage.getItem('client_id');
		this.formGroup = this.formBuilder.group({
			terms: [false, Validators.requiredTrue],
			certify: [false, Validators.requiredTrue]
		});
		let reqdata = {
			'auth': 'usman@2019',
			'client_id': this.clientId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};
		let req = {"auth": "usman@2019","authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe","client_id":client_id}
		this.apiService.waiversText(req).subscribe((res) => {
			this.waiverText = res['message'][0].waiver_text;
		});
	  
	}
	backClicked() {
		this._location.back();
	}

	submitForm(data){
		data = {
		    customers: []
		};
		
		let searchData = {
			'auth': 'usman@2019',
			'customerID': this.custId,
			'client_id': this.clientId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.searchCustomer(searchData).subscribe((response) => {
			if(response['status']=='success'){
				//data.customers = res['message'].child;
				data.customers.push({"CustomerID": this.custId,"IssuedBy_EmpNo":"2","TypeOfWaiver": "1","client_id" : this.clientId , "SignedByCustomerID": this.custId});
				console.log(response['message'].child);
				if(response['message'].child){
					for(var kk=0;kk<response['message'].child.length;kk++){
						data.customers.push({"CustomerID": response['message'].child[kk].CustomerID,"IssuedBy_EmpNo":"2","TypeOfWaiver": "1","client_id" : this.clientId , "SignedByCustomerID": this.custId});
						var waiverData = Object.assign(this.jsondata.reqData,data);
					}
					delete waiverData.customer_phone;
				}else{
					var waiverData = Object.assign(this.jsondata.reqData,data);
				}
				
				this.apiService.addWaivers(waiverData).subscribe((result) => {
					if(result['status']=='success'){
						this.router.navigate(['customer-jump']);
					}else{
						this.router.navigate(['customer-jump']);
						console.log(result);
					}
				});
			}else{
				console.log('Invalid User');
			}
		});
	}

}
