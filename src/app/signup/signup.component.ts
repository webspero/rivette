import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../rivette.service';
import { JsondataService } from '../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  customerdata;
  childdata;
  allChild;
  mergedData: any;
  formdata;
  gender;
  childDiv: boolean;
  moreChild: boolean;
  udpate: boolean;
  m_user: boolean;
  userExist: boolean;
  device_id;
  uniqueuid;
  show_waiver: any;
  is_m_user;
  allLocations;
  showForm:boolean;
  checkValid:boolean=false;

  showSpinner1:boolean=false;
  showSpinner2:boolean=false;


  constructor(
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private apiService: RivetteService,
      private jsondata: JsondataService,
      public toastController: ToastController,
      private http: HttpClient,
      private device: Device
  ) {
    this.formdata = new FormGroup({
    FirstName: new FormControl(null, Validators.required),
    LastName: new FormControl(null, Validators.required),
    EMailAddress: new FormControl(null, Validators.required),
    PhoneNumber: new FormControl('', Validators.required),
    chkPhoneNumber: new FormControl(),
    clientLocation: new FormControl(null, Validators.required)    
  });		
  }

  ngOnInit() {
    this.showForm =false;
    this.checkValid =false;
    // 
    let getLoc = {
      'auth': 'usman@2019',
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };
    this.apiService.masterClient(getLoc).subscribe((res) => {
      if(res['status']=='success'){
        this.allLocations = res['message'];
      }
      else{
        alert('Check Internet connection');
      }
    });
    // 
    // Get UUID

    if (!this.uniqueuid) {
      setTimeout(() => {
        this.uniqueuid = this.getUuid();
        if (this.uniqueuid) {
          localStorage.setItem('uuid', this.uniqueuid);
        }                
      }, 1000); 

      if (!this.uniqueuid) {
        setTimeout(() => {
          this.uniqueuid = this.getUuid();
          if (this.uniqueuid) {
            localStorage.setItem('uuid', this.uniqueuid);
          }
        }, 1000); 
      }
    }
    else{
      setTimeout(() => {
        this.uniqueuid = this.getUuid();
        if (this.uniqueuid) {
          localStorage.setItem('uuid', this.uniqueuid);
        }        
      }, 1000); 

      if (!this.uniqueuid) {
        setTimeout(() => {
          this.uniqueuid = this.getUuid();
          if (this.uniqueuid) {
            localStorage.setItem('uuid', this.uniqueuid);
          }
        }, 1000); 
      }
    }
    
    // END GET UUID
  }
  getUuid(){
    return this.device.uuid;
  }

  async nextForm(data = null){
    this.showSpinner1 = !this.showSpinner1;
    if(data){
      let phnNumber = data.PhoneNumber;
      let clientLoc = data.clientLocation;
      this.userDetails(phnNumber,clientLoc);   
    }
    
  }
  
  async prevForm(){
    this.showSpinner1 = !this.showSpinner1;
    this.showForm =!this.showForm;
  }
  

  fieldChange(data){
    if(data.detail.value){
      this.checkValid =true;
    }else{
      this.checkValid =false;
    }    
  }
  

  submitMembership(data){
    this.showSpinner2 = !this.showSpinner2;
    localStorage.setItem('pageType', 'member');
    this.show_waiver = false;
    if(localStorage.getItem('isValid') !='1' || localStorage.getItem('firstName') != data.FirstName || localStorage.getItem('lastName') != data.LastName){
      this.show_waiver = true;
    }

    localStorage.setItem('showWaiver', this.show_waiver);
    if(!this.userExist){      
      data.CustStatusID = 1;
      data.CustTypeID = 1;
      data.RelationshipType = 1;
      data.DaycareCustomerType = 1;
      data.Handicap = 1;
      data.device_id = this.uniqueuid;
      data.client_id = data.clientLocation;  
      data.Gender = this.gender;
      data.CustomAddress = '';
      let fcmToken = localStorage.getItem('firebaseToken');
      if(fcmToken){
        data.fcm_token = fcmToken;      
      }
      this.mergedData = Object.assign(this.jsondata.reqData, data);
      this.apiService.addCustomers(this.mergedData).subscribe((res) => {
        if(res['status']== 'success'){
            localStorage.setItem('custPhone', res['message'].customer.PhoneNumber);
            localStorage.setItem('client_id', res['message'].customer.client_id);
            localStorage.setItem('CustomerID', res['message'].customer.CustomerID);
            localStorage.setItem('stripe_acc', res['message'].stripe_acc);
            localStorage.setItem('publisher_key', res['message'].publisher_key);
            localStorage.setItem('custId', res['message'].customer.CustomerID);
            this.jsondata.storage = res['message'];
            this.router.navigate(['/rewards-home']);
        }else{
          this.showSpinner2 = !this.showSpinner2;
          alert(res['message']);
        }
      });
    }else{
      this.jsondata.storage = this.customerdata;
      localStorage.setItem('custPhone', this.customerdata.customer.PhoneNumber);
      localStorage.setItem('device_id', this.customerdata.customer.device_id);
      localStorage.setItem('custId', this.customerdata.customer.CustomerID);
      localStorage.setItem('client_id', this.customerdata.customer.client_id);
      localStorage.setItem('stripe_acc', this.customerdata.stripe_acc);
      localStorage.setItem('publisher_key', this.customerdata.publisher_key); 
      localStorage.setItem('CustomerID', this.customerdata.customer.CustomerID);
      this.router.navigate(['/rewards-home']);
    }
  }

  userDetails(custPhone = null, clientId = 1){
    let reqdata = {
      'auth': 'usman@2019',
      'customer_phone': custPhone,
      'client_id': clientId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };

    this.apiService.searchCustomer(reqdata).subscribe((res) => {
      console.log(res);
    if(res['status']=='success'){
        this.customerdata = res['message'];
        this.udpate = true;
        this.userExist = true;    
        this.gender = this.customerdata.Gender;
        
        localStorage.setItem('isValid', this.customerdata.customer.IsValid);
        localStorage.setItem('custPhone', this.customerdata.customer.PhoneNumber);
        localStorage.setItem('custData', this.customerdata.customer);
        localStorage.setItem('firstName', this.customerdata.customer.FirstName);
        localStorage.setItem('lastName', this.customerdata.customer.LastName);
        localStorage.setItem('CustomerID', this.customerdata.customer.CustomerID);
        localStorage.setItem('client_id', this.customerdata.customer.client_id);
        localStorage.setItem('custId', this.customerdata.customer.CustomerID);
        localStorage.setItem('stripe_acc', this.customerdata.stripe_acc);
        localStorage.setItem('publisher_key', this.customerdata.publisher_key); 
        let fcmToken = localStorage.getItem('firebaseToken');
        if(fcmToken){
          let fcmData = {
            "auth": "usman@2019",
            "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
            "customer_id":this.customerdata.customer.CustomerID,
            "client_id":this.customerdata.customer.client_id,
            "fcm_token":fcmToken
          };

          this.apiService.addFcm(fcmData).subscribe((res) => {
            if(res['status']=='success'){
              console.log(res);
            }
          });
        }
        if (this.customerdata.customer.m_user == "1") {
          this.m_user = true;
          let deviceData = {
            "auth": "usman@2019",
            "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
            "customer_id":this.customerdata.customer.CustomerID,
            "client_id":this.customerdata.customer.client_id,
            "device_id":this.uniqueuid
          }
          
          this.apiService.updateDevice(deviceData).subscribe((res) => {
            
            if(res['status']=='success'){
              this.router.navigate(['/rewards-home']);
            }else{
              this.router.navigate(['/rewards-home']);              
            }
          });
        } else {
          this.m_user = false;
          this.showForm =!this.showForm;
        }
        if (this.udpate || this.m_user) {
          this.formdata.controls['FirstName'].setValue(this.customerdata.customer.FirstName);
          this.formdata.controls['LastName'].setValue(this.customerdata.customer.LastName);
          this.formdata.controls['EMailAddress'].setValue(this.customerdata.customer.EMailAddress);
          this.formdata.controls['PhoneNumber'].setValue(this.customerdata.customer.PhoneNumber);
          localStorage.setItem('CustomerID', this.customerdata.customer.CustomerID);
          localStorage.setItem('client_id', this.customerdata.customer.client_id);
          localStorage.setItem('custId', this.customerdata.customer.CustomerID);
          localStorage.setItem('stripe_acc', this.customerdata.stripe_acc);
          localStorage.setItem('publisher_key', this.customerdata.publisher_key); 
        }
      }else {
        this.gender = 'M';
        this.udpate = false;
        this.userExist = false;
        this.m_user = false;
        this.showForm =!this.showForm;
      }
    });
  }
}
