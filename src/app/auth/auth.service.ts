import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHeaders } from  '@angular/common/http';
import { share, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtToken
	constructor(
		private myRoute: Router,
		private  httpClient:  HttpClient,
		private location: Location
	) { }

	sendValue(key: string,token: string) {
		localStorage.setItem(key, token)
	}
	
	getValue(key: string) {
		return localStorage.getItem(key)
  }
  
  deleteValue(key:string){
    localStorage.removeItem(key)
  }

	isLoggedIn() {
    var custId = this.getValue('custId');
    var client_id = this.getValue('client_id');
    if(custId && client_id){
      return true;
    }else{
      return false
    }
		
	}

}
