import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonFuncService } from '../../services/common/common-func.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss'],
})
export class OrderListComponent implements OnInit {
	all_data;
	orders;	
	activeSeg = 'active';
	noRecords:boolean;
	custId;
	client_id
  constructor(
	private apiService: RivetteService,
	private jsondata: JsondataService,
	private http: HttpClient,
	public actionSheetController: ActionSheetController,
	private CommonFunc: CommonFuncService
    ){ }

  	ngOnInit() {
		this.custId = localStorage.getItem('CustomerID');
		this.client_id = localStorage.getItem('client_id');		
	}
	ionViewWillEnter() {
		this.OrderHistory();
	}

	OrderHistory(){
		let reqdata = {
			'auth': 'usman@2019',
			'client_id':this.client_id,
			'customer_id' :this.custId,			
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe'
		};
		console.log(reqdata);
		this.apiService.OrderHistory(reqdata).subscribe((res) => {
			console.log(res);
			if(res['status']== 'success'){
				this.all_data = res['message'];
				this.orders = res['message']['order_info'];
			}else{
				this.noRecords = true;
				console.log(res['message']);
			}			
		});
	  } 
	custDate(dateTime){
		if(dateTime){
			var newDate = dateTime.replace(/ /g, "T");
			let date = new Date(newDate);
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
			];
			const weekDays = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			const getDay = weekDays[date.getDay()];
			const getMonth = monthNames[date.getMonth()];
			const getDate = date.getDate();
			
			return getDay+', '+getMonth+' '+getDate;
		}
	}
	cusTime(dateTime){
		var newDate = dateTime.replace(/ /g, "T");
		let date = new Date(newDate);
		const getHour = date.getHours();
		const getMinute = date.getMinutes();
		return getHour+':'+getMinute;	
	}
	getType(type){
		if(type){
			if(type == 1){
				return 'Jump Time';
			}
			if(type == 4){
				return 'Promotion';
			}
			if(type == 2){
				return 'Gift Card';
			}
			if(type == 3){
				return 'Jump Pass';
			}
			if(type == 5){
				return '';
			}
		}
		else{
			console.log('else');
		}
	}

	ifRedeemed(val){
		if(val == 'false'){
			return false;
		}
		if(val == 'true'){
			return true;
		}
	}
	selectButton(ev, custId){
		let selc = ev.detail.value;
		if(selc == 'camera'){
			this.CommonFunc.openCam(custId);
		}else if(selc == 'gallery'){
			this.CommonFunc.openGallery(custId);
		}else{

		}
	}
	openCam(){
		this.CommonFunc.openCam();
	}

	async uploadActionSheet(cId,index=null) {
		const actionSheet = await this.actionSheetController.create({
		  buttons: [{
			text: 'Camera',
			icon: 'camera',
			handler: () => {
        let profilePic:any = this.CommonFunc.openCam(cId);
        
        if(profilePic){
			this.OrderHistory();
        }else{
          console.log('Profile pic not found');
        } 
			}
		  }, {
			text: 'Open Gallery',
			icon: 'images',
			handler: () => {
        let profilePic:any = this.CommonFunc.openGallery(cId); 
        if(profilePic){
			this.OrderHistory();
        }else{          
          console.log('Profile pic not found');
        }
			}
		  }, {
			text: 'Cancel',
			icon: 'close',
			role: 'cancel',
		  }]
		});
		await actionSheet.present();
  	}
}
