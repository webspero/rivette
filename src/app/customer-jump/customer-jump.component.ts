import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { JsondataService } from "../jsondata.service";
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../rivette.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-customer-jump',
  templateUrl: './customer-jump.component.html',
  styleUrls: ['./customer-jump.component.scss'],
})
export class CustomerJumpComponent implements OnInit {
	details;
	data;
	merged_Data;
	custPhone;
	form;
	custId;
	clientId;
	userArray: Array<any> = [];
	jumpCust: Array<any> = [];
	pageType;
	titleType;
	goNext:boolean=false;

	constructor(
		private _location: Location,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private jsondata:  JsondataService,
		private apiService:  RivetteService,
		public toastController: ToastController,
		private http: HttpClient,
		private storage: Storage
	) {
		this.storage.remove('selected_plan');
		this.form = new FormGroup({
			jumpUser: new FormControl(false)
		});
		this.custId = localStorage.getItem('CustomerID');
		this.custPhone = localStorage.getItem('custPhone');
		this.pageType = localStorage.getItem('pageType');
		this.clientId = localStorage.getItem('client_id');		
		if(this.pageType != 'member'){
			this.titleType = 'Who all will be buying Jump tickets?'
		}else{
			this.titleType = 'Who all will be buying passes?'
		}
	}

	ngOnInit() {
		let customer_phone = {'customer_phone':this.custPhone};
		
		var mergedData = Object.assign(this.jsondata.reqData,customer_phone);
		
		let reqData = {	
					"auth": "usman@2019",
					"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
					"customerID": this.custId,
					"customer_id": this.custId,		
					"client_id": this.clientId,		
					"refill_card":"true",
					"reward_points":"true",
					"membership_status":"true"
				}
		
		this.apiService.customerDetails(reqData).subscribe((res) => {
			if(res['status'] == 'success'){
				console.log(res['message'].child);
				if(res['message'].child){
					this.jumpCust = res['message'].child;
				}
			}else{
				console.log('Something Went Wrong!!');
			}
			this.jumpCust.push(res['message'].customer);
			console.log(this.jumpCust)
		});
	
		// this.apiService.searchCustomer(mergedData).subscribe((res) => {
		// 	if(res['status'] == 'success'){
		// 		if(res['message'].child){
		// 			this.jumpCust = res['message'].child;
		// 		}
		// 	}else{
		// 		console.log('Something Went Wrong!!');
		// 	}
		// 	this.jumpCust.push(res['message'].customer);
		// 	console.log(this.jumpCust)
		// });
	}

	onChange(cid:string, isChecked: boolean, childname:string) {
		if(isChecked) {
			var index = this.userArray.indexOf(cid);
			this.userArray.splice(index,1);
		} else {
			this.userArray.push({name:childname,child:cid});
		}

		if(this.userArray.length > 0){
			this.goNext = true;
		}else{
			this.goNext = false;			
		}
	}
	
	submit(value) {
		if(this.userArray.length > 0){
			this.jsondata.waiverChild = this.userArray;
			localStorage.setItem('childSelect',JSON.stringify({"data":this.userArray}));
			if(this.pageType != 'member'){
				this.router.navigate(['purchase-jump']);
			}else{
				this.router.navigate(['/membership/select-membership']);
			}
			
		}else{
			console.log('No user selected');
		}		
	}

}
