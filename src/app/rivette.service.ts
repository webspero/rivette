import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RivetteService {
	API_URL  =  'https://app.rivette.ai/crmapi/v1.0/';
	custId = localStorage.getItem('custId');
	clientId = localStorage.getItem('client_id');		
	authData:any;
	authDataWithCustId:any;
	constructor(private  httpClient:  HttpClient) {	
		this.custId = localStorage.getItem('custId');
		this.clientId = localStorage.getItem('client_id');		
		this.authData  =  {
			"auth": "usman@2019",
			"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
			"client_id": this.clientId
		}
		this.authDataWithCustId = {
			"auth": "usman@2019",
			"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
			"customerID": this.custId,
			"customer_id": this.custId,		
			"client_id": this.clientId
		}
	}

	masterClient(customer){
		return  this.httpClient.post(this.API_URL+'master_client.php',customer);
	}

	paymentIntents(customer){
		return  this.httpClient.post(this.API_URL+'payment_intents_callback.php',customer);
	}

	piMembership(customer){
		return  this.httpClient.post(this.API_URL+'pi_membership.php',customer);
	}

	waiversText(reqdata){
		return  this.httpClient.post(this.API_URL+'liability_waivers.php',reqdata);
	}

	termsCondition(reqdata){
		return  this.httpClient.post(this.API_URL+'terms_condition.php',reqdata);
	}

	addWaivers(customer){
		return  this.httpClient.put(this.API_URL+'waivers_sign.php',customer);
	}

	addCustomers(customer){
		return this.httpClient.put(this.API_URL+'add_customers.php',customer);
	}

	addFcm(data){
		return this.httpClient.post(this.API_URL+'add_fcm.php',data);
	}

	updateDevice(data){
		return this.httpClient.post(this.API_URL+'update_device.php',data);
	}

	modifyCustomers(customer){
		return  this.httpClient.put(this.API_URL+'modify_customers.php',customer);
	}

	searchCustomer(customer){
		return  this.httpClient.post(this.API_URL+'search_customer.php',customer);
	}

	customerDetails(customer){
		return  this.httpClient.post(this.API_URL+'customer_details.php',customer);
	}

	addChild(customer){
		return  this.httpClient.put(this.API_URL+'add_child.php',customer);
	}

	modifyWaivers(customer){
		return  this.httpClient.post(this.API_URL+'modify_waivers.php',customer);
	}

	getSocks(customer){
		return  this.httpClient.post(this.API_URL+'inventories.php',customer);
	}

	OrderHistory(reqdata){
		return  this.httpClient.post(this.API_URL+'order_history.php',reqdata);
	}
	
	passesSummary(reqdata){		
		return  this.httpClient.post(this.API_URL+'passes_summary.php',reqdata);
	}
	
	rewardsPointSummary(reqdata){		
		return  this.httpClient.post(this.API_URL+'rewards_point_summary.php',reqdata);
	}
	

	getInventories(customer){
		return  this.httpClient.post(this.API_URL+'inventories.php',customer);
	}
	
	getMemberships(customer){
		return  this.httpClient.post(this.API_URL+'membership.php',customer);
	}

	getCampaigns(customer){
		return  this.httpClient.post(this.API_URL+'special_campaign.php',customer);
	}

	doTransaction(customer){
		return  this.httpClient.post(this.API_URL+'do_transaction.php',customer);
	}

	getGiftcards(customer){
		return  this.httpClient.post(this.API_URL+'giftcards.php',customer);
	}
	
	redeemGiftCoupon(data){
		return  this.httpClient.post(this.API_URL+'gift_card_redeem.php',data);
	}

	doTransactionReward(data){
		return  this.httpClient.post(this.API_URL+'do_transaction.php',data);
	}

	refillCards(data){
		return  this.httpClient.post(this.API_URL+'refillcards.php',data);
	}

	addRefillCard(data){
		return  this.httpClient.post(this.API_URL+'add_refill_card.php',data);
	}
	
	getRewardRules(data){
		return  this.httpClient.post(this.API_URL+'get_reward_rules.php',data);
	}

	rewardPoints(data){
		return  this.httpClient.post(this.API_URL+'reward_points.php',data);
	}

	notifications(data){
		return  this.httpClient.post(this.API_URL+'notifications.php',data);
	}

	updateNotifications(data){
		return  this.httpClient.post(this.API_URL+'update_notifications.php',data);
	}
	
	profilePhoto(data){
		return  this.httpClient.post(this.API_URL+'profile_photo.php',data);
	}
	
}
