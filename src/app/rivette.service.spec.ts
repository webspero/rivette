import { TestBed } from '@angular/core/testing';

import { RivetteService } from './rivette.service';

describe('RivetteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RivetteService = TestBed.get(RivetteService);
    expect(service).toBeTruthy();
  });
});
