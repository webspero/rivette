import { Component, OnInit } from '@angular/core';
import { Device } from '@ionic-native/device/ngx';
import { RivetteService } from  '../rivette.service';
import { Router, ActivatedRoute} from '@angular/router';
import { JsondataService } from "../jsondata.service";
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.page.html',
  styleUrls: ['./home-screen.page.scss'],
})
export class HomeScreenPage implements OnInit {

  uniqueuid;
  fcmToken;
  custId = localStorage.getItem('CustomerID');
  clientId = localStorage.getItem('client_id');

  constructor(
		private router: Router,
		private device: Device,
		private apiService:  RivetteService,
		private jsondata:  JsondataService,
    private http: HttpClient
  ) {
    this.custId = localStorage.getItem('CustomerID');
    this.clientId = localStorage.getItem('client_id');
  }

  ngOnInit() {
    
    localStorage.clear();
    if (!this.uniqueuid) {
      setTimeout(() => {
        this.uniqueuid = this.getUuid();
        if (this.uniqueuid) {
          localStorage.setItem('uuid', this.uniqueuid);
          this.getData(this.uniqueuid);
        }                
      }, 1000); 

      if (!this.uniqueuid) {
        setTimeout(() => {
          this.uniqueuid = this.getUuid();
          if (this.uniqueuid) {
            localStorage.setItem('uuid', this.uniqueuid);
            this.getData(this.uniqueuid);
          }
        }, 1000); 
      }
    }
    else{
      setTimeout(() => {
        this.uniqueuid = this.getUuid();
        if (this.uniqueuid) {
          localStorage.setItem('uuid', this.uniqueuid);
          this.getData(this.uniqueuid);
        }        
      }, 1000); 

      if (!this.uniqueuid) {
        setTimeout(() => {
          this.uniqueuid = this.getUuid();
          if (this.uniqueuid) {
            localStorage.setItem('uuid', this.uniqueuid);
            this.getData(this.uniqueuid);
          }
        }, 1000); 
      }
    }
  }

  getUuid(){
    return this.device.uuid;
  }
  
  async getData(device_id){    
    let reqdata = {
      'auth': 'usman@2019',
			'device_id': device_id,
			'client_id': this.clientId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };
    console.log(reqdata);
		let httpHeaders = new HttpHeaders({
			'Content-Type' : 'application/json',
		});
		this.apiService.searchCustomer(reqdata).subscribe((res) => {
      console.log(res);
			if(res['status']=='success'){
          this.jsondata.storage = res['message'];
          localStorage.setItem('isValid', res['message'].customer.IsValid);
          localStorage.setItem('firstName', res['message'].customer.FirstName);
          localStorage.setItem('lastName', res['message'].customer.LastName);
          localStorage.setItem('custPhone', res['message'].customer.PhoneNumber);
          localStorage.setItem('CustomerID', res['message'].customer.CustomerID);
          localStorage.setItem('client_id', res['message'].customer.client_id);
          localStorage.setItem('stripe_acc', res['message'].stripe_acc);
          localStorage.setItem('publisher_key', res['message'].publisher_key);
          localStorage.setItem('custId', res['message'].customer.CustomerID);
          localStorage.setItem('deviceToken', device_id);
          this.router.navigate(['rewards-home']);
          let fcmToken = localStorage.getItem('firebaseToken');
          if(fcmToken){
            let fcmData = {
              "auth": "usman@2019",
              "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
              "client_id":res['message'].customer.client_id,
              "customer_id":res['message'].customer.CustomerID,
              "fcm_token":fcmToken
            };    
            this.apiService.addFcm(fcmData).subscribe((res) => {
              if(res['status']=='success'){
                console.log(res);
              }
            });
          }
			}else{
        delete this.jsondata.storage;
        this.router.navigate(['signup']);
			}
		}); 
	}

}
