import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController,ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonFuncService } from '../../services/common/common-func.service';

declare var Stripe;

@Component({
  selector: 'app-membership-payment',
  templateUrl: './membership-payment.component.html',
  styleUrls: ['./membership-payment.component.scss'],
})
export class MembershipPaymentComponent implements OnInit {
    planPrice;
    formdata;
    pipe;
    formVal;
    membershipId;
    listofDays:any;
    card: any;    
    clientSecret;
	qrcode;
	qrcodeImg;
	coupon;
	allcoupons;
	customerdata;
	expire_date;
	showsuccess: boolean;
	showpayment: boolean;
	showspinner: boolean;
	elements;
	cardElement;
	memberList;
	piPlanData;
	
	useremail;
	userPhone;
	userName;
	userAddress;

	custId;

	stripe;

	clientId = localStorage.getItem('client_id');
	
	stripeAccount = localStorage.getItem('stripe_acc');
	publisher_key = localStorage.getItem('publisher_key');

	constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient,
        private CommonFunc: CommonFuncService,
		private storage: Storage,
		public actionSheetController: ActionSheetController
		
    ) {
		this.clientId = localStorage.getItem('client_id');    	
    }

	async ngOnInit() {
		this.stripeAccount = localStorage.getItem('stripe_acc');
		this.publisher_key = localStorage.getItem('publisher_key');
		console.log(this.memberList);
		this.storage.get('selected_plan').then((val) => {
			this.memberList = val;
			console.log(this.memberList);
		});
		this.storage.get('pi_planData').then((planData) => {
			this.piPlanData = planData;
			console.log(this.piPlanData);
		});
		  
		this.stripe = Stripe(this.publisher_key, {
			betas: ['payment_intent_beta_3'],
			stripeAccount: this.stripeAccount
		});
	  
		this.elements = this.stripe.elements(); 

		this.cardElement = this.elements.create('card', {
			iconStyle: 'solid',
			style: {
			base: {
				iconColor: '#8898AA',
				color: 'black',
				lineHeight: '36px',
				fontWeight: 300,
				fontFamily: 'Helvetica Neue',
				fontSize: '19px',
				'::placeholder': {
				color: '#8898AA',
				},
			},
			invalid: {
				iconColor: '#e85746',
				color: '#e85746',
			}
			},
			classes: {
				focus: 'is-focused',
				empty: 'is-empty',
			},
		});  

		console.log(this.cardElement);
		this.custId = localStorage.getItem('CustomerID');
		this.showpayment = true;
		this.showspinner = false;
		
		this.planPrice = this.activatedRoute.snapshot.paramMap.get("price");
  		
		this.formdata = new FormGroup({
			radio: new FormControl(null,Validators.required)
		});
		
		// GET CUSTOMER 

		let CustID = localStorage.getItem('CustomerID');
		let ClientID = localStorage.getItem('client_id'); 
		console.log(CustID);
		console.log(ClientID);

		let reqdata = {
	      'auth': 'usman@2019',
	      'customerID': CustID,
	      'client_id': ClientID,
	      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
	    };
	    this.apiService.searchCustomer(reqdata).subscribe((res) => {
	    	if(res['status']=='success'){
				this.customerdata = res['message'];
				this.useremail = this.customerdata.customer.EMailAddress;
				this.userPhone = this.customerdata.customer.PhoneNumber;
				this.userName = this.customerdata.customer.FirstName+' '+this.customerdata.customer.LastName;
				this.userAddress = this.customerdata.customer.Address1+' '+this.customerdata.customer.Address2;

			if(!this.custId){
				this.custId = this.customerdata.customer.CustomerID;
			}
				this.cardElement.mount('#card-element');
	        	console.log(this.customerdata);
	      	}else {
	      		console.log('cust not found');
	      	}
		});
		
		// END GET CUSTOMER		
	}

	async uploadActionSheet(cId,index=null) {
		const actionSheet = await this.actionSheetController.create({
		  buttons: [{
			text: 'Camera',
			icon: 'camera',
			handler: () => {
        let profilePic:any = this.CommonFunc.openCam(cId);
        
        if(profilePic){
          console.log('inside profilePic');
          this.allcoupons[index].profile_pic = profilePic;           
        }else{
          console.log('Profile pic not found');
        } 
			}
      }, 
      {
		text: 'Open Gallery',
		icon: 'images',
		handler: () => {
			let profilePic:any = this.CommonFunc.openGallery(cId); 
			if(profilePic){
			console.log('inside profilePic');
			this.allcoupons[index].profile_pic = profilePic+'?newImg='+this.getTimeStamp();
			}else{          
			console.log('Profile pic not found');
			}
			}
		}, {
			text: 'Cancel',
			icon: 'close',
			role: 'cancel',
		  }]
		});
		await actionSheet.present();
  	}

	
	getTimeStamp(){
		return new Date().getTime();
	}

	async presentToast() {
	    const toast = await this.toastController.create({
	      message: 'Your settings have been saved.',
	      duration: 2000
	    });
	    toast.present();
  	}

  	viewDetails(custid){
  		console.log('custid:' + custid);
  		this.router.navigate(['/membership-overview/',custid]);
  	}


	makePayment(clientSecret){
		this.showspinner = true;
		/** get all this details from UI **/
		var plan_id = "plan_F9lAlExrkvGhh5";
		var qty = 2;

		var newThis = this;
		var sourceData = {
			owner: {
				 name: this.userName, //cust Name
				 address: { //cust Address
					 line1: this.userAddress
				 },
				 email: this.useremail, //cust email
				 phone: this.userPhone //cust phone
			 }
		 };
		 

		this.stripe.createSource(this.cardElement, sourceData).then(function(result) {
			if (result.error) {
				console.log('testas error');
				// Inform the customer that there was an error.
				 var errorElement = document.getElementById('card-errors');
				 errorElement.textContent = result.error.message;
				 console.log(result);
			  } else {
				console.log('testas noError');
				console.log(result);
				
				let newPiData = {
					"auth": "usman@2019",
					"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
					"name": newThis.userName,
					"email": newThis.useremail,
					"stripe_account_id": newThis.stripeAccount,
					"plans":JSON.parse(newThis.piPlanData),
					"stripe_source": result.source.id
				}
				console.log(newPiData);
				newThis.apiService.piMembership(newPiData).subscribe((res) => {
					if(res['status']=='success'){
						var plan_clientSecret = res['message'].latest_invoice.payment_intent.client_secret;
						var subID = res['message'].id;
						newThis.stripe.handleCardPayment(plan_clientSecret).then(function(result) {
							console.log(result);
							console.log('es');
							if (result.error) {
								newThis.showspinner = false;
								alert(result.error.message);
							}
							else if(result.paymentIntent && result.paymentIntent.status === 'succeeded') {
								console.log('response to check');
								console.log(result);
								let paymentIntentid = result.paymentIntent.id;
								localStorage.setItem('tranId',result.paymentIntent.id);
								var paymentIntentStatus = result.paymentIntent.status;
								if (paymentIntentStatus == 'succeeded') {
									newThis.showspinner = false;
									newThis.showsuccess = true;
									newThis.showpayment = false;
									let doTransacdata = newThis.doTransac(paymentIntentid,subID);
								}
			
							  }else {
								console.log('result->');
								console.log(result);
							}
					  });
					}else{
						console.log('error');
						console.log(res);
					}
					
				});
			  }			
		});
		return;
    	
	}

	doTransac(paymentIntentid,subID){
		// Do Transaction
		console.log(JSON.parse(this.memberList));

		let transactionData = {
				'auth': 'usman@2019',
				'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
				'client_id': this.clientId,
				'customer_id': this.custId,
				'amount': this.planPrice,
				'transection_id': paymentIntentid,
				'order_type': '3',
				'stripe_member':subID,
				'orders' : JSON.parse(this.memberList)
			};
		let httpHeaders = new HttpHeaders({
			'Content-Type' : 'application/json',
		});

		console.log(transactionData);
		this.apiService.doTransaction(transactionData).subscribe((res) => {
			if(res['status']=='success'){
				console.log(res);
				this.showsuccess = true;
				this.showpayment = false;
				this.qrcode = res['message'].qrcode;
				this.qrcodeImg = res['message'].qrcode_img;
				this.allcoupons = res['message'].coupons;
				return(res['message']);
			}else{
				alert('error');
			}
		});
	};

	async setupStripe(planPrice,userName = null,useremail = null){
		console.log(planPrice);
		var paymentIntent;
	    
	    // this.cardElement.mount('#card-element');

	    let reqdata = {
			'auth': 'usman@2019',
			'stripe_account_id': this.stripeAccount,
			'amount': planPrice,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.piMembership(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				console.log('success');
				this.clientSecret = res['message'].client_secret;
			}else{
				console.log('error');
				console.log(res);
			}
			console.log();
		});
	  }
	  
	uploadPic(cId,index=null){
	console.log(this.allcoupons[index]);
	let profilePic = this.CommonFunc.openCam(cId);
	this.allcoupons[index].profile_pic = profilePic;
	
	}
	fromGallery(cId,index=null){
	console.log(cId);
	let profilePic = this.CommonFunc.openGallery(cId);    
	this.allcoupons[index].profile_pic = profilePic;
	}

}