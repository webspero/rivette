import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-members-info',
  templateUrl: './members-info.component.html',
  styleUrls: ['./members-info.component.scss'],
})
export class MembersInfoComponent implements OnInit {
    customerdata;
    childdata;
    allChild;
    mergedData: any;
    formdata;
    gender;
    childDiv: boolean;
    moreChild: boolean;
    udpate: boolean;
    m_user: boolean;
    userExist: boolean;
    show_waiver: any;
	is_m_user;
	
	custId;
    client_id;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
    	this.formdata = new FormGroup({
			FirstName: new FormControl(null, Validators.required),
			LastName: new FormControl(null, Validators.required),
			EMailAddress: new FormControl(null, Validators.required),
			PhoneNumber: new FormControl(null, Validators.required)
		});		
    }

    ngOnInit() {
		this.custId = localStorage.getItem('CustomerID');
    	this.client_id = localStorage.getItem('client_id');
		let reqdata = {
			'auth': 'usman@2019',
			'customerID':this.custId,
			'client_id': this.client_id,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};
		this.apiService.searchCustomer(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				this.customerdata = res['message'];
				this.udpate = true;
				this.userExist = true;
				this.gender = this.customerdata.Gender;
				if (this.is_m_user == "1") {
					this.m_user = true;
				} else {
					this.m_user = false;
				}
				console.log(this.customerdata);
				localStorage.setItem('isValid', this.customerdata.customer.IsValid);
           	 	localStorage.setItem('firstName', this.customerdata.customer.FirstName);
				localStorage.setItem('lastName', this.customerdata.customer.LastName);
				if (this.udpate || this.m_user) {
					this.formdata.controls['FirstName'].setValue(this.customerdata.customer.FirstName);
					this.formdata.controls['LastName'].setValue(this.customerdata.customer.LastName);
					this.formdata.controls['EMailAddress'].setValue(this.customerdata.customer.EMailAddress);
					this.formdata.controls['PhoneNumber'].setValue(this.customerdata.customer.PhoneNumber);
				}
			}else {
				console.log(reqdata);
				console.log(res);
				this.gender = 'M';
				this.udpate = false;
				this.userExist = false;
				this.m_user = false;
			}
		});
    }
	
	
	submitMembership(data){
		localStorage.setItem('pageType', 'member');
		this.show_waiver = false;
		console.log(data);
		if(localStorage.getItem('isValid') !='1' || localStorage.getItem('firstName') != data.FirstName || localStorage.getItem('lastName') != data.LastName){
            this.show_waiver = true;
        }

		localStorage.setItem('showWaiver', this.show_waiver);
		
		if(	
			localStorage.getItem('isValid') !='1' || 
			localStorage.getItem('firstName') != data.FirstName || 
			localStorage.getItem('lastName') != data.LastName || 
			this.customerdata.customer.EMailAddress != data.EMailAddress || 
			this.customerdata.customer.PhoneNumber != data.PhoneNumber
			){
				console.log('inside');
				this.show_waiver = true;
				
				let reqdata = {
						"auth": "usman@2019",
						"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
						"CustomerID":this.custId,
						"client_id":this.client_id,
						"FirstName": data.FirstName,
						"LastName":data.LastName,
						"PhoneNumber": data.PhoneNumber,
						"EMailAddress":data.EMailAddress
					}
				
				this.apiService.modifyCustomers(reqdata).subscribe((res) => {
					if(res['status']=='success'){
						var modifiedData = res['message'];
						console.log(modifiedData);
						localStorage.setItem('firstName', modifiedData.FirstName);
						localStorage.setItem('lastName', modifiedData.LastName);
						localStorage.setItem('custPhone', modifiedData.PhoneNumber);
						localStorage.setItem('EMailAddress', modifiedData.EMailAddress);
					
						this.customerdata.customer.FirstName = modifiedData.FirstName;
						this.customerdata.customer.LastName = modifiedData.LastName;
						this.customerdata.customer.EMailAddress = modifiedData.EMailAddress;
						this.customerdata.customer.PhoneNumber = modifiedData.PhoneNumber;

						this.jsondata.storage = this.customerdata;
						this.router.navigate(['/customer-child']);
					}else {
						this.jsondata.storage = this.customerdata;
						this.router.navigate(['/customer-child']);
					}
				});
        }else{
			console.log('ELSE inside');
			this.jsondata.storage = this.customerdata;
			localStorage.setItem('custPhone', this.customerdata.customer.PhoneNumber);
            localStorage.setItem('custId', this.customerdata.customer.CustomerID);
			this.router.navigate(['/customer-child']);			
		}
		
	}
}