import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-select-membership',
  templateUrl: './select-membership.component.html',
  styleUrls: ['./select-membership.component.scss'],
})
export class SelectMembershipComponent implements OnInit {
  membershipPlan;
  formdata;
  formVal;
  membershipId;
  selectedChild;
  checkedVal: Array<any> = [];
  totalVal;
  showpay;
  selectedPass;
  custId;
  client_id;
  // test
  customAlertOptions: any = {
  	header: 'Memberships',
  	subHeader: 'Select your membership',
  	translucent: true
  };

// test
	constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient,
        private storage: Storage,
        public auth: AuthService,
    ) {
      this.custId = localStorage.getItem('CustomerID');
      this.client_id = localStorage.getItem('client_id');
      
      let reqdata = {
        'auth': 'usman@2019',
			  'client_id': this.client_id,
        'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      };
    
      this.apiService.getMemberships(reqdata).subscribe((res) => {
        this.membershipPlan = res['message'];
        this.defaultCheck();
      });

    }
	
	ngOnInit() {
    
    this.selectedPass = this.auth.getValue('selectedPass');

    this.checkedVal = [];
		localStorage.removeItem('member_id');
		this.selectedChild = JSON.parse(localStorage.getItem('childSelect'));
    this.formdata = new FormGroup({
        member_Plan: this.initChild()
    });
  }

  defaultCheck(){
    if(this.membershipPlan){
      for(var a=0;a<this.membershipPlan.length;a++){
        if(this.membershipPlan[a].product_id === this.selectedPass){
          for(var k=0;k<this.selectedChild.data.length;k++){
              var price = this.membershipPlan[a].m_price;
              var pid = this.membershipPlan[a].pid;
              var child_id = this.selectedChild.data[k].child;
              var plan_id = this.membershipPlan[a].plan_id;
              //this.checkedVal.push({price:price,plan_id:pid,child_id:child_id,pId:plan_id});
              this.onItemChange(true,price,plan_id,child_id,pid);
            }
            
            
        }
      }
    }
    console.log(this.checkedVal);
  }



  initChild() : FormArray {
    const roomsArray = new FormArray([]);
    for(let i = 0; i < 1; i++) {
      const roomGroup = new FormGroup({
        memberPlan: new FormControl('', Validators.required)
      });

    roomsArray.push(roomGroup);
    }
    return roomsArray;
  }
 
  onItemChange(isChecked, price,pId=null,childId,planId){
    if(isChecked) {
      if(this.checkedVal.length){
        for (var i=0; i<this.checkedVal.length; i++) {
          if (this.checkedVal[i].child_id == childId) {
            delete this.checkedVal[i]
          }
        }

        this.checkedVal.push({price:price,plan_id:planId,child_id:childId,pId:pId});
        //remove empty json
        this.checkedVal = this.checkedVal.filter(function( element ) {
          return element !== undefined;
        });
      }else{
        console.log('Notloop');
        this.checkedVal.push({price:price,plan_id:planId,child_id:childId,pId:pId });
      }
     // this.checkedVal.push({price:price,plan_id:planId,child_id:childId});
    } else {
      var index = this.checkedVal.indexOf(childId);
      this.checkedVal.splice(index,1);
    }
    this.getSum();
    console.log(this.checkedVal);
  }

  getSum(){
    const result = [];
    const piPlanData = [];
    const piPlanQnty = [];
    const total = [];
    const map = new Map();
    
    for (const item of this.checkedVal) {
      if(!map.has(item.child_id)){
          map.set(item.child_id, true);    // set any value to Map
          total.push(+(parseFloat(item.price).toFixed(2)));
          result.push(
            {
              membership_id: item.plan_id,"quantity": "1",jumpers:[{jumper_id:item.child_id}]}
          );
          
          piPlanData.push(item.pId);
      }
    }
    // GET IT
    piPlanData.sort();
    var current = null;
    var cnt = 0;
    for (var i = 0; i < piPlanData.length; i++) {
      if (piPlanData[i] != current) {
          if (cnt > 0) {
            piPlanQnty.push(
              {
                plan_id: current,"quantity": cnt
              }
            );
          }
          current = piPlanData[i];
          cnt = 1;
      } else {
          cnt++;
      }
    }
    if (cnt > 0) {
      piPlanQnty.push(
        {
          plan_id: current,"quantity": cnt
        }
      );
      console.log(current + ' comes --> ' + cnt + ' times');
    }
    // END get PlanId
    console.log(result);
    console.log(piPlanQnty);
    this.storage.set('selected_plan', JSON.stringify(result));
    this.storage.set('pi_planData', JSON.stringify(piPlanQnty));
    
    this.totalVal = total.reduce((a, b) => a + b, 0);
    this.totalVal = this.totalVal.toFixed(2);
    this.showpay = '$'+this.totalVal;
  }

	submitVal(val){
    this.router.navigate(['/membership/membership-payment',this.totalVal]);
	}

}
