import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { RivetteService } from '../../rivette.service';
import { CommonFuncService } from '../../services/common/common-func.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-select-prepass',
  templateUrl: './select-prepass.component.html',
  styleUrls: ['./select-prepass.component.scss'],
})
export class SelectPrepassComponent implements OnInit {
  membershipPlan:any;
  passesSummary;
  selectedPass;
  expanded:boolean=false;
  custId;
  clientId;
  noPasses:boolean=false;

  imageData:any='';
  image:any='';

  picUpldCustId;

  checkedVal;
  radioItem;
  localProductId:boolean=false;

  constructor(
    private camera: Camera,
    private apiService: RivetteService,
    public actionSheetController: ActionSheetController,
    private loadingController: LoadingController,
    private transfer: FileTransfer,
    private CommonFunc: CommonFuncService,
    public auth: AuthService,
  ) {
    this.auth.deleteValue('selectedPass');
    this.custId = localStorage.getItem('custId');
	  this.clientId = localStorage.getItem('client_id');
    let reqdata = {
			'auth': 'usman@2019',
			'client_id': this.clientId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      "customerID": this.custId,
			"customer_id": this.custId
    };
    console.log(reqdata);
		this.apiService.getMemberships(reqdata).subscribe((res) => {
      this.membershipPlan = res['message'];
      console.log(this.membershipPlan);
    });
    
    
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.passSummary();
  }

  getTimeStamp(){
    return new Date().getTime();
  }

  passSummary(){
    let reqdata = {
			'auth': 'usman@2019',
			'client_id': this.clientId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      "customerID": this.custId,
			"customer_id": this.custId
    };
    this.apiService.passesSummary(reqdata).subscribe((res) => {
      console.log(res);
      this.passesSummary = res['message'].order_info;
      if(!this.passesSummary){
        this.noPasses=true;
      }
		});
  }

  segmentChanged(ev){
    this.selectedPass = ev.detail.value;
    console.log(this.selectedPass);
    localStorage.setItem('selectedPass', this.selectedPass);
    let setPass = localStorage.getItem('selectedPass');
  }

  // uploadPic(cId,index=null){
  //   console.log(this.passesSummary[index]);
  //   let profilePic:any = this.CommonFunc.openCam(cId);
    
  //   if(profilePic){
  //     console.log('profilePic:' + profilePic);   
  //     this.passesSummary[index].profile_pic = profilePic;
  //   }else{
  //     console.log('Profile pic not found');
  //   } 
  // }
  // fromGallery(cId,index=null){
  //   console.log(cId);
  //   let profilePic:any = this.CommonFunc.openGallery(cId); 
  //   if(profilePic){
  //     console.log('profilePic:' + profilePic);   
  //     this.passesSummary[index].profile_pic = profilePic;
  //   }else{
      
  //     console.log('Profile pic not found');
  //   }
  // }

  async uploadActionSheet(cId,index=null) {
		const actionSheet = await this.actionSheetController.create({
		  buttons: [{
			text: 'Camera',
			icon: 'camera',
			handler: () => {
        let profilePic:any = this.CommonFunc.openCam(cId);
        console.log('handler camera');
        console.log(profilePic);        
        if(profilePic){
          console.log('inside profilePic');          
			    this.passesSummary[index].profile_pic = profilePic+'?newImg='+this.getTimeStamp();
        }else{
          // this.passSummary();
          console.log('Profile pic not found');
        } 
        console.log('kk');
			}
      }, 
      {
			text: 'Open Gallery',
			icon: 'images',
			handler: () => {
        let profilePic:any = this.CommonFunc.openGallery(cId); 
        console.log('handler');
        console.log(profilePic);
        if(profilePic){
          console.log('inside profilePic');
          // this.passSummary(); 
			    this.passesSummary[index].profile_pic = profilePic+'?newImg='+this.getTimeStamp();
        }else{          
          console.log('Profile pic not found');
        }
        console.log('kk');
			}
		  }, {
			text: 'Cancel',
			icon: 'close',
			role: 'cancel',
		  }]
		});
    await actionSheet.present();     
  }

  openCam(cid=null,index=null){
    this.picUpldCustId = cid;
    const options: CameraOptions = {
      quality: 70,
      allowEdit: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageData=imageData
      this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
      this.upload(index);
    }, (err) => {
      // Handle error
      alert("error "+JSON.stringify(err))
    });

  }
  openGallery(cid=null,index=null){
    this.picUpldCustId = cid;		
    const options: CameraOptions = {
      quality: 70,
      allowEdit: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageData=imageData
      this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
      this.upload(index);
    }, (err) => {
      // Handle error
      alert("error "+JSON.stringify(err))
    });
  
  }

  async upload(index=null)
  {	
    let photo_data = {
      'auth': 'usman@2019',			
      'client_id':this.clientId,
      'CustomerID':this.picUpldCustId,
      'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
      'profile_pic' : this.imageData,
    };

    const loading = await this.loadingController.create({
      message: 'Uploading...',
      });
    await loading.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options1: FileUploadOptions = {
      fileKey: 'profile_pic',
      fileName: 'name.jpg',
      httpMethod: 'POST',
      chunkedMode: false,
      headers: {
        Authorization:'4c1f77c8cc97a3b21dbbbff7655e29fe'
      },
      params: photo_data
    }
	
    fileTransfer.upload(this.imageData, 'https://app.rivette.ai/crmapi/v1.0/profile_photo.php', options1)
    .then((data) => {
      // success
      console.log(data);
      loading.dismiss();
      alert("Image Uploaded");
      let profilePic = data['message'].profile_pic_url;
      if(profilePic){
        console.log('inside profilePic');
        this.passSummary(); 
        this.passesSummary[index].profile_pic = profilePic;
      }else{
        alert('not uploaded');
      }
    }, (err) => {
      // error
      loading.dismiss();
      alert("error"+JSON.stringify(err));
    });
  }
  
  onItemChange(isChecked=null, planId=null){
    if(isChecked) {
      localStorage.setItem('selectedPass', planId);
      this.localProductId = true;
    } else {
      console.log('else');
    }
  } 
  
}
