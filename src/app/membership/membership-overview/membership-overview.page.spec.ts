import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembershipOverviewPage } from './membership-overview.page';

describe('MembershipOverviewPage', () => {
  let component: MembershipOverviewPage;
  let fixture: ComponentFixture<MembershipOverviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembershipOverviewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembershipOverviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
