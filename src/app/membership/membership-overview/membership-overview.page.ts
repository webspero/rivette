import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
 
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';

import { Router, ActivatedRoute } from '@angular/router';

import { finalize } from 'rxjs/operators';
 
const STORAGE_KEY = 'my_images';

@Component({
  selector: 'app-membership-overview',
  templateUrl: './membership-overview.page.html',
  styleUrls: ['./membership-overview.page.scss'],
})
export class MembershipOverviewPage implements OnInit {
 
  images = [];
  custid;
  customerdata;
  image:any=''
  imageData:any=''
  pickimages:any[]=[]
  profilePic;
  custId = localStorage.getItem('CustomerID');
  clientId = localStorage.getItem('client_id'); 
  constructor(
  	private camera: Camera,
	private file: File,
	private http: HttpClient,
	private webview: WebView,
	private transfer: FileTransfer,

	private actionSheetController: ActionSheetController,
	private toastController: ToastController,

	private storage: Storage,
	private plt: Platform,
	private loadingController: LoadingController,

	private ref: ChangeDetectorRef,
	private filePath: FilePath,
	private imagePicker: ImagePicker,

	private activatedRoute: ActivatedRoute,
    private router: Router,

    private apiService: RivetteService,
    private jsondata: JsondataService,
	)
	{ 	
		this.custId = localStorage.getItem('CustomerID');
    	this.clientId = localStorage.getItem('client_id');
		this.custid = this.activatedRoute.snapshot.paramMap.get("custid");
		let reqdata = {
	      'auth': 'usman@2019',
	      'customerID': this.custid,
	      'client_id': this.clientId,
	      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
	    };
	    this.apiService.searchCustomer(reqdata).subscribe((res) => {
	    if(res['status']=='success'){
			this.customerdata = res['message'];
	        console.log(this.customerdata);
	      }else {
	      	console.log('cust not found');
	      }
	    });
	}
	openImagePicker()
	{
		let options={
		maximumImagesCount:10
		}

		this.imagePicker.getPictures(options).then((results) => {
		
		for (var i = 0; i < results.length; i++) {
			results[i]=(<any>window).Ionic.WebView.convertFileSrc(results[i])
		}
		this.pickimages=results
		}, (err) => { });
	}

	openCam(){

		const options: CameraOptions = {
		  quality: 70,
		  allowEdit: true,
		  destinationType: this.camera.DestinationType.FILE_URI,
		  encodingType: this.camera.EncodingType.JPEG,
		  mediaType: this.camera.MediaType.PICTURE
		}
		
		this.camera.getPicture(options).then((imageData) => {
		 // imageData is either a base64 encoded string or a file URI
		 // If it's base64 (DATA_URL):
		 this.imageData=imageData
		 this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
		 this.upload();
		}, (err) => {
		 // Handle error
		 alert("error "+JSON.stringify(err))
		});
	
	  }

	  openGallery(){

		const options: CameraOptions = {
		  quality: 70,
		  allowEdit: true,
		  destinationType: this.camera.DestinationType.FILE_URI,
		  encodingType: this.camera.EncodingType.JPEG,
		  mediaType: this.camera.MediaType.PICTURE,
		  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
		}
		
		this.camera.getPicture(options).then((imageData) => {
		 // imageData is either a base64 encoded string or a file URI
		 // If it's base64 (DATA_URL):
		 this.imageData=imageData
		 this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
		 this.upload();
		}, (err) => {
		 // Handle error
		 alert("error "+JSON.stringify(err))
		});
	
	  }
	  
	
	
	  async upload()
	  {	
		let photo_data = {
			'auth': 'usman@2019',			
			'client_id': this.clientId,
			'CustomerID':this.custid,
			'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
			'profile_pic' : this.imageData,
		};

		const loading = await this.loadingController.create({
		  message: 'Uploading...',
		  });
		await loading.present();
	
		const fileTransfer: FileTransferObject = this.transfer.create();
	
		let options1: FileUploadOptions = {
			fileKey: 'profile_pic',
			fileName: 'name.jpg',
			httpMethod: 'POST',
			headers: {
				Authorization:'4c1f77c8cc97a3b21dbbbff7655e29fe'
			},
			params: photo_data
		}

		
	
	fileTransfer.upload(this.imageData, 'https://app.rivette.ai/crmapi/v1.0/profile_photo.php', options1)
	 .then((data) => {
	   // success
	   console.log(data);
	   loading.dismiss()
	   alert("Image Uploaded");
	   this.profilePic = data['message'].profile_pic_url;
	 }, (err) => {
	   // error
	   alert("error"+JSON.stringify(err));
	 });
	}
 
	ngOnInit() {
		this.plt.ready().then(() => {
		  this.loadStoredImages();
		});
	}
	 
	loadStoredImages() {
		this.storage.get(STORAGE_KEY).then(images => {
		  if (images) {
		    let arr = JSON.parse(images);
		    this.images = [];
		    for (let img of arr) {
		      let filePath = this.file.dataDirectory + img;
		      let resPath = this.pathForImage(filePath);
		      this.images.push({ name: img, path: resPath, filePath: filePath });
		    }
		  }
		});
	}
	 
	pathForImage(img) {
		if (img === null) {
		  return '';
		} else {
		  let converted = this.webview.convertFileSrc(img);
		  return converted;
		}
	}
	 
	async presentToast(text) {
		const toast = await this.toastController.create({
		    message: text,
		    position: 'bottom',
		    duration: 3000
		});
		toast.present();
	}
	 
	  // Next functions follow here...
	  async selectImage() {
	    const actionSheet = await this.actionSheetController.create({
	        header: "Select Image source",
	        buttons: [{
	                text: 'Load from Library',
	                handler: () => {
	                    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
	                }
	            },
	            {
	                text: 'Use Camera',
	                handler: () => {
	                    this.takePicture(this.camera.PictureSourceType.CAMERA);
	                }
	            },
	            {
	                text: 'Cancel',
	                role: 'cancel'
	            }
	        ]
	    });
	    await actionSheet.present();
	}
 
	takePicture(sourceType: PictureSourceType) {
	    var options: CameraOptions = {
	        quality: 100,
	        sourceType: sourceType,
	        saveToPhotoAlbum: false,
	        correctOrientation: true
	    };
	 
	    this.camera.getPicture(options).then(imagePath => {
	        if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
	            this.filePath.resolveNativePath(imagePath)
	                .then(filePath => {
	                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
	                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
	                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
	                });
	        } else {
	            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
	            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
	            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
	        }
	    });
	 
	}

	createFileName() {
	    var d = new Date(),
	        n = d.getTime(),
	        newFileName = n + ".jpg";
	    return newFileName;
	}
	 
	copyFileToLocalDir(namePath, currentName, newFileName) {
	    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
	        this.updateStoredImages(newFileName);
	    }, error => {
	        this.presentToast('Error while storing file.');
	    });
	}
	 
	updateStoredImages(name) {
	    this.storage.get(STORAGE_KEY).then(images => {
	        let arr = JSON.parse(images);
	        if (!arr) {
	            let newImages = [name];
	            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
	        } else {
	            arr.push(name);
	            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
	        }
	 
	        let filePath = this.file.dataDirectory + name;
	        let resPath = this.pathForImage(filePath);
	 
	        let newEntry = {
	            name: name,
	            path: resPath,
	            filePath: filePath
	        };
	 
	        this.images = [newEntry, ...this.images];
	        this.ref.detectChanges(); // trigger change detection cycle
	    });
	}
	deleteImage(imgEntry, position) {
	    this.images.splice(position, 1);
	 
	    this.storage.get(STORAGE_KEY).then(images => {
	        let arr = JSON.parse(images);
	        let filtered = arr.filter(name => name != imgEntry.name);
	        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));
	 
	        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
	 
	        this.file.removeFile(correctPath, imgEntry.name).then(res => {
	            this.presentToast('File removed.');
	        });
	    });
	}

	// Inspired by https://golb.hplar.ch/2017/02/Uploading-pictures-from-Ionic-2-to-Spring-Boot.html
 
	startUpload(imgEntry) {
	    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => this.readFile(file))
        })
        .catch(err => {
            this.presentToast('Error while reading file.');
        });
	}
	 
	readFile(file: any) {
	    const reader = new FileReader();
	    reader.onloadend = () => {
	        const formData = new FormData();
	        const imgBlob = new Blob([reader.result], {
	            type: file.type
	        });
	        formData.append('file', imgBlob, file.name);
	        this.uploadImageData(formData);
	    };
	    reader.readAsArrayBuffer(file);
	}
	 
	async uploadImageData(formData: FormData) {
	    const loading = await this.loadingController.create({
	        // content: 'Uploading image...',
	    });
	    await loading.present();
	 
	    this.http.post("https://app.rivette.ai/crmapi/v1.0/profile_photo.php", formData)
	        .pipe(
	            finalize(() => {
	                loading.dismiss();
	            })
	        )
	        .subscribe(res => {
	        	console.log(res);
	            if (res['success']) {
	                this.presentToast('File upload complete.')
	            } else {
	                this.presentToast('File upload failed.')
	            }
	        });
	}
 
}
