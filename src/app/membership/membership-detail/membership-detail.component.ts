import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-membership-detail',
	templateUrl: './membership-detail.component.html',
	styleUrls: ['./membership-detail.component.scss'],
})
export class MembershipDetailComponent implements OnInit {
	firstname;
	lastname;
	address;
	custid;
	email;
	member_id;
	custPhone;
	selectchild;
	reqdata;
	planName;
	planPrice;

	constructor(
		private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
	) { }

	ngOnInit() {
		console.log(this.jsondata.reqData);
		this.selectchild = JSON.parse(localStorage.getItem("childSelect"));
		this.member_id = localStorage.getItem("member_id");
		this.custPhone = localStorage.getItem("custPhone");
		this.reqdata = this.jsondata.reqData;
		this.reqdata.customer_phone = this.custPhone;
		this.apiService.searchCustomer(this.reqdata).subscribe((res) => {
			this.firstname = res['message'].customer.FirstName;
			this.email = res['message'].customer.EMailAddress;
			this.address = res['message'].customer.Address1+' '+res['message'].customer.Address2;
		});
		this.apiService.getMemberships(this.reqdata).subscribe((res) => {
			console.log(res);
			for (let key in res['message']) {
			    if(res['message'][key].pid == this.member_id){
			    	this.planName = res['message'][key].membership_name;
			    	this.planPrice = res['message'][key].m_price;
			    	//this.planId = res['message'][key].pid;
			    }
			}
		});
	}

}
