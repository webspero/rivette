import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { DatePipe } from '@angular/common';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-selected-membership',
  templateUrl: './selected-membership.component.html',
  styleUrls: ['./selected-membership.component.scss'],
})
export class SelectedMembershipComponent implements OnInit {
    membershipPlan;
    planName;
    planPrice;
    planId;
    formdata;
    pipe;
    formVal;
    membershipId;
    listofDays:any;
	data: any;
	custId;
	client_id;
	constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
		this.pipe = new DatePipe('en-US');
		this.custId = localStorage.getItem('CustomerID');
		this.client_id = localStorage.getItem('client_id');
		let reqdata = {
			'auth': 'usman@2019',
			'customerID':this.custId,
			'client_id': this.client_id,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};
		let selectedId = localStorage.getItem('member_id');
		this.apiService.getMemberships(reqdata).subscribe((res) => {
			console.log(res);
			for (let key in res['message']) {
			    if(res['message'][key].pid == selectedId){
			    	this.planName = res['message'][key].membership_name;
			    	this.planPrice = res['message'][key].m_price;
			    	this.planId = res['message'][key].pid;
			    }
			}
		});
    }
	
	ngOnInit() {
		this.listofDays =  [
	            {"key":"30 Days", "value":30},
	            {"key":"60 Days", "value":60},
	            {"key":"90 Days", "value":90}
	        ]	
		this.formdata = new FormGroup({
			selectDays: new FormControl(-1),
			endDate: new FormControl(null),
			startDate: new FormControl(null),
		});
		
    }

    resetDays(){
    	this.formdata.controls['selectDays'].setValue(-1);
    }

    resetDate(){
    	this.formdata.controls['startDate'].reset();
    	this.formdata.controls['endDate'].reset();
    }

	submitVal(val){
		
		if(val.selectDays < 0 && val.startDate!=null && val.endDate!=null ){
			const start_Date = this.pipe.transform(val.startDate, 'yyyy-MM-dd');
			const end_Date = this.pipe.transform(val.endDate, 'yyyy-MM-dd');
			this.jsondata.paymentVal = {'startdate':start_Date,'endate':end_Date};
			this.router.navigate(['/membership/membership-payment']);
		}else if(val.selectDays > 0){
			console.log(val.selectDays);
			this.jsondata.paymentVal = {'totalDays':val.selectDays};
			this.router.navigate(['/membership/membership-payment']);
		}else{
			console.log('Something went wrong!!');
		}
		
	}

}
