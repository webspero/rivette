import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { JsondataService } from "../jsondata.service";
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../rivette.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-purchase-jump',
  templateUrl: './purchase-jump.component.html',
  styleUrls: ['./purchase-jump.component.scss'],
})
export class PurchaseJumpComponent implements OnInit {
	details; 
	data;
	merged_Data;
	custPhone;
	formdata;
	socksData;
	catData;
	custId;
	showdiv;
	subCategory;
	socksCost;
	Total_value;
	userArray: Array<any> = [];
	socksList: any;
	catList: Array<any> = [];
	subcatList: Array<any> = [];
	custList: Array<any> = [];
	payList: Array<any> = [];
	childSelect;
	catCost;
	sum;
	socksQty;
	payVal;
	invData;
	sockData;

	constructor(
		private _location: Location,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private jsondata:  JsondataService,
		private apiService:  RivetteService,
		public toastController: ToastController,
		private http: HttpClient,
		private storage: Storage
	) {

		this.formdata = new FormGroup({
			quantity: new FormControl(null),
		});

		this.custId = localStorage.getItem('custId');
		this.custPhone = localStorage.getItem('custPhone');
		
		this.childSelect = JSON.parse(localStorage.getItem('childSelect'));
		
		this.catData = this.jsondata.reqData;
		this.apiService.getSocks(this.catData).subscribe((res) => {
			console.log(res);
			if(res['status'] == 'success'){
				console.log(res['message'].Socks);
				console.log(res['message'].Jump_Time);
				this.catList = res['message'].Jump_Time;
				// res['message'].forEach(element => {
				// 	console.log(element);
				// });
				// var k = 0;
				// if(res['message'].regular_inventories){
				// 	for(var i =0;i<res['message'].regular_inventories.length;i++){
				// 		res['message'].regular_inventories[i].price = parseFloat(res['message'].regular_inventories[i].price).toFixed(2);
				// 		this.catList[k] = res['message'].regular_inventories[i];
				// 		k = k+1;
				// 	}
				// }

				// if(res['message'].socks_inventories){
				// 	for(var j =0;j<res['message'].socks_inventories.length;j++){
				// 		console.log(res['message'].socks_inventories[j].Description);
				// 		if(res['message'].socks_inventories[j].Description == 'Socks'){
				// 			this.socksList = res['message'].socks_inventories[j];
				// 		}
				// 	}
				// 	console.log('this.socksList');
				// 	console.log(this.socksList);
				// }
			}
		},(err)=>{console.log(err)});
	}

	ngOnInit() {
		
	}

	filterCost(val){
		//val = 15.25
		if(val % 1 == 0){
			return '$'+parseInt(val);
		}else{
			return '$'+val;
		}
		
	}


	fieldChange(allVal,val) {
		let field_id = allVal.detail.value;
		field_id = field_id.split("_")
		const cost = field_id[1];
		if(this.custList.length){
			for (var i=0; i<this.custList.length; i++) {
	          if (this.custList[i].child_id == val) {
	            delete this.custList[i]
	          }
	        }

	        this.custList.push({price:field_id[1],field_id:field_id[0],child_id:val});
	        //remove empty json
	    }else{
	    	this.custList.push({price:field_id[1],field_id:field_id[0],child_id:val});
	    }
	    this.calcCatCost(cost)
	    this.totalCost()
	}

	togglediv(val){
		this.showdiv = val;
	}

	socksVal(value){
		this.socksQty = value.detail.value
		this.totalCost()
	}

	calcCatCost(catID){
		this.custList = this.custList.filter(function( element ) {
          return element !== undefined;
        });
	}

	//Calculate ALl Values on Dropdown changes
	totalCost(){
		this.catCost = 0;
		this.sum = 0;
		console.log(this.custList)
		for(var kk=0;kk<this.custList.length;kk++){
			this.sum = parseInt(this.custList[kk].price)
			this.catCost = parseInt(this.sum)+parseInt(this.catCost);
		}

		this.catCost = parseFloat(this.catCost).toFixed(2);

		if(this.socksList != undefined){
			this.socksCost = this.socksQty * this.socksList.price;
			if(!this.socksCost){
				this.socksCost = parseFloat('0').toFixed(2);
			}
		}else{
			this.socksCost = parseFloat('0').toFixed(2);
		}
		this.Total_value = parseFloat(this.catCost)+parseFloat(this.socksCost);
		this.payVal = '$'+this.Total_value.toFixed(2);

	}

	submit(formValue){
		if(this.custList.length){
			for(var kk=0;kk<this.custList.length;kk++){
				this.payList.push({amount:this.custList[kk].price,inventory_id:this.custList[kk].field_id,quantity:1});
			}
		}
		console.log(this.payList);
		this.storage.set('jump_plan', JSON.stringify(this.payList));
		this.router.navigate(['/jump-payment/',this.Total_value.toFixed(2)]);
	}

}
