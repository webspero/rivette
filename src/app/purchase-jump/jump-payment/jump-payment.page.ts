import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

declare var Stripe;

@Component({
  selector: 'app-jump-payment',
  templateUrl: './jump-payment.page.html',
  styleUrls: ['./jump-payment.page.scss'],
})
export class JumpPaymentPage implements OnInit {
	planPrice;
	formdata;
	pipe;
	formVal;
	membershipId;
	listofDays:any;
	card: any;    
	clientSecret;
	qrcode;
	qrcodeImg;
	coupon;
	allcoupons;
	customerdata;
	expire_date;
	showsuccess: boolean;
	showpayment: boolean;
	showspinner: boolean;
	elements;
	cardElement;
	memberList;
	
	useremail;
	userPhone;
	userName;
	userAddress;

	stripe;

	custId;
	client_id;

	stripeAccount = localStorage.getItem('stripe_acc');
	publisher_key = localStorage.getItem('publisher_key');

	constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient,
        private storage: Storage
    ) {
    	
    }

	ngOnInit() {
		this.custId = localStorage.getItem('CustomerID');
		this.client_id = localStorage.getItem('client_id');
		console.log(this.memberList);
		this.stripeAccount = localStorage.getItem('stripe_acc');
		this.publisher_key = localStorage.getItem('publisher_key');
		this.storage.get('jump_plan').then((val) => {
    		this.memberList = val;
  		});
		this.stripe = Stripe(this.publisher_key, {
    	betas: ['payment_intent_beta_3'],
    	stripeAccount: this.stripeAccount
  	});
		this.elements = this.stripe.elements(); 

    this.cardElement = this.elements.create('card', {
	    iconStyle: 'solid',
	    style: {
	      base: {
	        iconColor: '#8898AA',
	        color: 'black',
	        lineHeight: '36px',
	        fontWeight: 300,
	        fontFamily: 'Helvetica Neue',
	        fontSize: '19px',
	        '::placeholder': {
	          color: '#8898AA',
	        },
	      },
	      invalid: {
	        iconColor: '#e85746',
	        color: '#e85746',
	      }
	    },
	    classes: {
	      focus: 'is-focused',
	      empty: 'is-empty',
	    },
	  });  
		this.showpayment = true;
		this.showspinner = false;
		
		this.planPrice = this.activatedRoute.snapshot.paramMap.get("price");
  		
		this.formdata = new FormGroup({
			radio: new FormControl(null,Validators.required)
		});

		this.setupStripe(this.planPrice);
		// GET CUSTOMER 
		let reqdata = {
	      'auth': 'usman@2019',
		  'customerID':this.custId,
		  'client_id': this.client_id,
	      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
	    };
	    this.apiService.searchCustomer(reqdata).subscribe((res) => {
	    if(res['status']=='success'){
			this.customerdata = res['message'];
			this.useremail = this.customerdata.customer.EMailAddress;
			this.userPhone = this.customerdata.customer.PhoneNumber;
			this.userName = this.customerdata.customer.FirstName+' '+this.customerdata.customer.LastName;
			this.userAddress = this.customerdata.customer.Address1+' '+this.customerdata.customer.Address2;

	        console.log(this.customerdata);
	      }else {
	      	console.log('cust not found');
	      }
	    });
		// END GET CUSTOMER
		
    }

	async presentToast() {
	    const toast = await this.toastController.create({
	      message: 'Your settings have been saved.',
	      duration: 2000
	    });
	    toast.present();
  	}

  	viewDetails(custid){
  		console.log('custid:' + custid);
  		this.router.navigate(['/membership-overview/',custid]);
  	}


	makePayment(clientSecret){
		this.showspinner = true;
		console.log('makePayment');
		console.log(clientSecret);
		
		let newThis = this;
					
    	this.stripe.handleCardPayment(clientSecret, this.cardElement, {
				source_data: {
				   owner: {
						name: this.userName, //cust Name
						address: { //cust Address
							line1: this.userAddress
						},
						email: this.useremail, //cust email
						phone: this.userPhone //cust phone
					}
				},
		  	}).then(function(result) {
		  		if (result.error) {
							newThis.showspinner = false;
							alert(result.error.message);
	        	}
		        else if(result.paymentIntent && result.paymentIntent.status === 'succeeded') {
				    console.log(result);
			        let paymentIntentid = result.paymentIntent.id;
			        localStorage.setItem('tranId',result.paymentIntent.id);
			        var paymentIntentStatus = result.paymentIntent.status;
			        if (paymentIntentStatus == 'succeeded') {
								newThis.showspinner = false;
			        	newThis.showsuccess = true;
			        	newThis.showpayment = false;
			        	let doTransacdata = newThis.doTransac(paymentIntentid);
			        }

			  	}else {
			        console.log('result');
				}
      	});
	}

	doTransac(paymentIntentid){
		// Do Transaction
		let transactionData = {
			'auth': 'usman@2019',
			'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
			'client_id': this.client_id,
			'customer_id': this.custId,
			'amount': '12',
			'transection_id': paymentIntentid,
			'order_type': '1',
			'orders' : JSON.parse(this.memberList)
		};

		let httpHeaders = new HttpHeaders({
			'Content-Type' : 'application/json',
		});

		//console.log(transactionData);
		this.apiService.doTransaction(transactionData).subscribe((res) => {
			if(res['status']=='success'){
				this.showsuccess = true;
				this.showpayment = false;
				this.qrcode = res['message'].qrcode;
				this.qrcodeImg = res['message'].qrcode_img;
				this.allcoupons = res['message'].coupons;
				return(res['message']);
			}else{
				alert('error');
			}
		});
			// End Do Transaction
	};

	async setupStripe(planPrice){
		console.log(planPrice);
		var paymentIntent;
	    
	    this.cardElement.mount('#card-element');

	    let reqdata = {
			'auth': 'usman@2019',
			'stripe_account_id': this.stripeAccount,
			'amount': planPrice,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.paymentIntents(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				console.log('success');
				console.log(res);
				this.clientSecret = res['message'].client_secret;
			}else{
				console.log('error');
				console.log(res);
			}
			console.log();
		});

  	}


}
