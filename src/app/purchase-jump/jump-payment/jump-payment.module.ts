import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { JumpPaymentPage } from './jump-payment.page';

const routes: Routes = [
  {
    path: '',
    component: JumpPaymentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [JumpPaymentPage]
})
export class JumpPaymentPageModule {}
