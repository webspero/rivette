import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-campaign-buy',
	templateUrl: './campaign-buy.component.html',
	styleUrls: ['./campaign-buy.component.scss'],
})
export class CampaignBuyComponent implements OnInit {
	campaigns;
    formdata;
    formVal;
	membershipId;
	ifHasNoCamp:boolean=false;
    client_id;
	constructor(
		private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
    	
		
    }

	ngOnInit() {
		
		/*this.formdata = new FormGroup({
			memberPlan: new FormControl(null, Validators.required)
		});*/

	}
	ionViewWillEnter(){
		// your code to initialize
		this.client_id = localStorage.getItem('client_id');
		let reqdata = {
			'auth': 'usman@2019',
			'client_id': this.client_id,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};
		this.apiService.getCampaigns(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				this.campaigns = res['message'];
				if(typeof(this.campaigns) === 'string'){
					console.log(this.campaigns);
					this.ifHasNoCamp = true;									
				}
			}else{				
				this.ifHasNoCamp = true;
								
			}
		});
	 }

}