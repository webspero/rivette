import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


declare var Stripe;

@Component({
  selector: 'app-campaign-payment',
  templateUrl: './campaign-payment.component.html',
  styleUrls: ['./campaign-payment.component.scss'],
})
export class CampaignPaymentComponent implements OnInit {
	planPrice;
	formdata;
	pipe;
	formVal;
	membershipId;
	campId;
	offerData;
	listofDays:any;
	card: any;    
	clientSecret;
	qrcode;
	qrcodeImg;
	coupon;
	allData;
	allcoupons;
	customerdata;
	expire_date;
	showsuccess: boolean;
	showpayment: boolean;
	showspinner: boolean;
	elements;
	cardElement;

	useremail;
	userPhone;
	userName;
	userAddress;

	totalConvFee;

	stripe;
	ifStripe:boolean = true;
	walletBalance:number = 0;
	redeemRefill:boolean;
	amountDue;
	refillPaymentType = null;
	custId = localStorage.getItem('CustomerID');
	clientId = localStorage.getItem('client_id');
	
	stripeAccount = localStorage.getItem('stripe_acc');
	publisher_key = localStorage.getItem('publisher_key');

	constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
		this.custId = localStorage.getItem('CustomerID');
    	this.clientId = localStorage.getItem('client_id');
    	
    }
	
	ngOnInit() {
		this.stripeAccount = localStorage.getItem('stripe_acc');
		this.publisher_key = localStorage.getItem('publisher_key');

		this.stripe = Stripe(this.publisher_key, {
	    	betas: ['payment_intent_beta_3'],
	    	stripeAccount: this.stripeAccount
  		});
		this.elements = this.stripe.elements(); 

	    this.cardElement = this.elements.create('card', {
		    iconStyle: 'solid',
		    style: {
		      base: {
		        iconColor: '#8898AA',
		        color: 'black',
		        lineHeight: '36px',
		        fontWeight: 300,
		        fontFamily: 'Helvetica Neue',
		        fontSize: '19px',
		        '::placeholder': {
		          color: '#8898AA',
		        },
		      },
		      invalid: {
		        iconColor: '#e85746',
		        color: '#e85746',
		      }
		    },
		    classes: {
		      focus: 'is-focused',
		      empty: 'is-empty',
		    },
		  });  
		this.showpayment = true;
		this.showspinner = false;
		
		// this.planPrice = this.activatedRoute.snapshot.paramMap.get("price");
		this.planPrice = this.jsondata.totalAmount;
		this.amountDue = this.planPrice;
		this.campId = this.activatedRoute.snapshot.paramMap.get("id");
  		
		this.formdata = new FormGroup({
			radio: new FormControl(null,Validators.required)
		});

		this.setupStripe(this.amountDue);
		// GET CUSTOMER 
		
		let CustID = localStorage.getItem('CustomerID');
		let ClientID = localStorage.getItem('client_id'); 

		let reqdata = {
	      'auth': 'usman@2019',
	      'customerID': CustID,
	      'client_id': ClientID,
		  'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		  'refill_card':true
	    };
	    this.apiService.searchCustomer(reqdata).subscribe((res) => {
	    if(res['status']=='success'){
			this.customerdata = res['message'];
			this.useremail = this.customerdata.customer.EMailAddress;
			this.userPhone = this.customerdata.customer.PhoneNumber;
			this.userName = this.customerdata.customer.FirstName+' '+this.customerdata.customer.LastName;
			this.userAddress = this.customerdata.customer.Address1+' '+this.customerdata.customer.Address2;
			
			if (res['message'].refill_card && res['message'].refill_card.length) {
				this.walletBalance = res['message'].refill_card[0].total_amount;
			}else{

			}
	      }else {
	      	console.log('cust not found');
	      }
	    });
		// END GET CUSTOMER
		let storageData = this.jsondata.storage;
		this.offerData = Object.values(storageData);
		this.totalConvFee = this.jsondata.totalConvFee;
				
		console.log(this.totalConvFee);
		
    }

	
	redeemToggle(ev: any){
		console.log(ev);
		console.log(ev.detail.checked);
	}

	refillCheck(ev: any){
		
		let ifChecked = ev.detail.checked;
		
		if(ifChecked){
			let dueAmount = (+this.planPrice) - (+this.walletBalance);
			if(dueAmount > 0){
				this.refillPaymentType = 'partial';
				dueAmount = Math.abs(dueAmount);
				console.log(typeof(dueAmount));
				if(dueAmount % 1 != 0){
					this.amountDue = dueAmount.toFixed(2);				
				}else{
					this.amountDue = Math.abs(dueAmount);				
				}
				
				this.setupStripe(this.amountDue);
				this.ifStripe = true;
			}else{
				this.refillPaymentType = 'full';
				this.amountDue = 0;
				this.ifStripe = false;
			}
			console.log(this.refillPaymentType);
		}else{
			this.refillPaymentType = '';
			this.amountDue = (+this.planPrice);
			this.setupStripe(this.amountDue);
			this.ifStripe = true;
			console.log(this.refillPaymentType);
		}
		

		
	}

	makePayment(clientSecret){
		this.showspinner = true;
		
		let newThis = this;

    	this.stripe.handleCardPayment(clientSecret, this.cardElement, {
				source_data: {
				   owner: {
						name: this.userName,
						address: {
							line1: this.userAddress
						},
						email: this.useremail,
						phone:this.userPhone
					}
				},
		  	}).then(function(result) {
		  		if (result.error) {
					newThis.showspinner = false;
					alert(result.error.message);
	        	}
		        else if(result.paymentIntent && result.paymentIntent.status === 'succeeded') {
			        let paymentIntentid = result.paymentIntent.id;
			        localStorage.setItem('tranId',result.paymentIntent.id);
			        var paymentIntentStatus = result.paymentIntent.status;
			        if (paymentIntentStatus == 'succeeded') {
						newThis.showspinner = false;
			        	newThis.showsuccess = true;
			        	newThis.showpayment = false;
			        	let doTransacdata = newThis.doTransac(paymentIntentid);
			        }

			  	}else {
			        console.log('result');
				}
      	});
	}

	doTransac(paymentIntentid,transactionType = null){
		// Do Transaction
		console.log(this.planPrice);
		console.log(this.walletBalance);
		if (typeof this.planPrice != "number") {
			this.planPrice = +(this.planPrice);
		}		
		
		if(this.refillPaymentType == 'full' ||  this.refillPaymentType == 'partial'){
			if(this.planPrice > this.walletBalance){
				console.log('walletBal');
				var refill_amount:number = this.walletBalance;
			}else if(this.planPrice <= this.walletBalance){
				console.log('planPriceBal');
				var refill_amount:number = this.planPrice;	
			}
			var transactionData:{} = {
				'auth': 'usman@2019',
				'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
				'client_id': this.clientId,
				'customer_id': this.customerdata.customer.CustomerID,
				'amount': this.planPrice,
				"refill_payment_type":this.refillPaymentType,
				"refill_amount":refill_amount,				
				'transection_id': paymentIntentid,
				'order_type': '4',
				'specal_camp_id':this.campId,
				
				"total_convenience_fee":this.totalConvFee,
				'orders' : this.offerData					
			};
		}else{
			var transactionData:{} = {
				'auth': 'usman@2019',
				'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
				'client_id': this.clientId,
				'customer_id': this.customerdata.customer.CustomerID,
				'amount': this.planPrice,
				'transection_id': paymentIntentid,
				'order_type': '4',
				'specal_camp_id':this.campId,
				
				"total_convenience_fee":this.totalConvFee,
				'orders' : this.offerData					
			};
		}
		
		console.log(transactionData);

		let httpHeaders = new HttpHeaders({
			'Content-Type' : 'application/json',
		});

		this.apiService.doTransaction(transactionData).subscribe((res) => {
			if(res['status']=='success'){
				console.log(res);
				this.showsuccess = true;
				this.showpayment = false;
				this.allData = res['message'];
				this.qrcode = res['message'].qrcode;
				this.qrcodeImg = res['message'].qrcode_img;
				this.allcoupons = res['message'].coupons;
				return(res['message']);
			}else{
				console.log(res);
				alert('error');
			}
		});
	};

	async setupStripe(planPrice){
		console.log('setupStripe');
		console.log(this.refillPaymentType);
		var paymentIntent;
	    
	    this.cardElement.mount('#promotion-card-element');

	    let reqdata = {
			'auth': 'usman@2019',
			'stripe_account_id': this.stripeAccount,
			'amount': planPrice,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.paymentIntents(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				this.clientSecret = res['message'].client_secret;
			}else{
				console.log(res);
			}
		});

  	}
}
