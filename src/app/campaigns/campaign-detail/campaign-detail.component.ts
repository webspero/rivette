import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html',
  styleUrls: ['./campaign-detail.component.scss'],
})
export class CampaignDetailComponent implements OnInit {
	campaigns: { coupon_per_customer: string; id: any; };
	campid: string;
  formdata: any;
  formVal: any;
  membershipId: any;
  disabled = false;
  positiveAmt = false;
  totalamount:number = 0;
  showAddbtn = '-1';
  all_data = [];
  cust_data = [];
  qty_of_value;
  final_array = [];
  fianlarray = [];
  totalValue =[];
  qnty: number[];
  totalQuantity =[];
  qntyCount: number;
  qntChk:boolean=false;
  totalQnty;
  convenienceFeeType:any;
  ifCampgnActive:boolean=false;
  custId = localStorage.getItem('CustomerID');
  clientId = localStorage.getItem('client_id');
    
  public selectedPacks: Array<any> = [];

  constructor(
		private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
      this.custId = localStorage.getItem('CustomerID');
      this.clientId = localStorage.getItem('client_id');  		
	}

  ngOnInit() {
    localStorage.setItem('checkQty','0');
    this.campid = this.activatedRoute.snapshot.paramMap.get("id");
    let reqdata = {
      'auth': 'usman@2019',
      'client_id': this.clientId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      "camp_id":this.campid,
    };
		this.apiService.getCampaigns(reqdata).subscribe((res) => {
      this.campaigns = res['message'];
      this.convenienceFeeType = res['message'].convenience_fee_type;
      let cpnPerCust= this.campaigns.coupon_per_customer;
      if(!cpnPerCust){
        this.ifCampgnActive = true;
      }
      this.qntyCount =  parseInt(this.campaigns.coupon_per_customer);
      let camp_qnt = this.qntyCount + 1;  
      this.qnty = Array(camp_qnt).fill(0).map((x,i)=>i);
		});

  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
 
  onOptionsSelected(value,price: string,pacId:string,pacName:string){
    this.qty_of_value = localStorage.getItem('checkQty');
    this.qty_of_value = parseInt(this.qty_of_value)+parseInt(value)
   
    if(this.qty_of_value > this.qntyCount){
      console.log('qty_of_value > this.qntyCount')
    }

    this.totalQnty= parseInt(price)*parseInt(value); 
    this.cust_data.push({offer_id:pacId,total_cost:this.totalQnty,qty:parseInt(value)});
    if(this.totalQnty > this.qntyCount){
      this.qntChk= true;
    }

    // Remove Duplicates
    this.final_array = this.cust_data.reduceRight(function (r, a) {
      r.some(function (b) { return a.offer_id === b.offer_id; }) || r.push(a);
      return r;
    }, []);

  
    this.fianlarray=[];
    this.totalValue = [];
    this.totalQuantity = [];
    
    var k= 0;
    for (var i=0; i<this.final_array.length; i++) {
      if (this.final_array[i].total_cost>0) {
        this.totalValue[k] = this.final_array[i].total_cost;
        this.totalQuantity[k] = this.final_array[i].qty;
        this.fianlarray[k] = this.final_array[i];
        k++;
      }
    }
  
    this.totalamount = this.totalValue.reduce((a, b) => a + b, 0)
    localStorage.setItem('checkQty',this.totalQuantity.reduce((a, b) => a + b, 0));
    
    let priceInt = parseInt(price);
    
    let pacKey = 'id'+pacId;
    let totalPackPrice = priceInt*value;
    

    if(totalPackPrice >= 1 && totalPackPrice <= 100){
    	var convenienceFee = 1.69;
		}else if(totalPackPrice > 100 && totalPackPrice <= 200 ){
			var convenienceFee = 2.49;			
		}
		else if(totalPackPrice > 200){
      var convenienceFee = 4.99;			
    }
    
    let selectedPack = {
      'offer_id':  pacId,
      'quantity':  value,
      'amount':  priceInt,
      'convenience_fee':  convenienceFee,
      'convenience_fee_type':  this.convenienceFeeType,            
    }
    
    this.selectedPacks[pacKey] = selectedPack;
    if(this.selectedPacks.length){
        for (var i=0; i<this.selectedPacks.length; i++) {
        console.log('inside');
        console.log(this.selectedPacks[i]);
        if (this.selectedPacks[i] == pacKey) {
          console.log('inside I go');
        }
      }
      this.selectedPacks[pacKey] = selectedPack;
      
    }else{
      const index: number = this.selectedPacks.indexOf(pacKey);
      
      if (value == 0) {
        delete this.selectedPacks[pacKey];        
      }else{    
        console.log('non-zero');
        this.selectedPacks[pacKey] = selectedPack;
      }
    }
    
    var finalConvFee = 0;
    for (let key in this.selectedPacks) {
      let value = this.selectedPacks[key];
      finalConvFee = finalConvFee + value.convenience_fee;
      
    }
    
    if(this.convenienceFeeType == 1){
      this.totalamount = this.totalamount + finalConvFee;
    }
    
    this.selectedPacks.forEach(pack => {
      console.log('here');      
    });
    
    this.jsondata.storage = this.selectedPacks;
    this.jsondata.totalConvFee = finalConvFee;
    this.jsondata.totalAmount = this.totalamount;

    
  }

  async makePayment(price: string){
    if(parseInt(price) <= 0){
      this.presentToast('Please Select Quantity');
    }else{
      this.router.navigate(['/campaign-payment',parseInt(price),this.campaigns.id]);
    }
  }
  async addPayment(price: number,c_id: string){
    this.showAddbtn = c_id;
    let c_price:number = price;
    this.totalamount =  +c_price + this.totalamount;
  }
  async removePayment(price: any,c_id: string){
    this.showAddbtn = c_id;
    
  }

  public onChange(event: any) {
    this.disabled = !this.disabled;
    // if(this.totalamount < 1){
    //   console.log(true);
    //   this.disabled = false;   
    // }
  }

  async onChangeCamp(event: any,price: number,campg_id: string) { 
    
    var dataobject = {};
    let c_price:number = price;

    let c_id:string = campg_id;
    if(!event) {
      if(this.all_data.length){
        for (var i=0; i<this.all_data.length; i++) {
          if (this.all_data[i].campg_id == campg_id) {
            delete this.all_data[i]
          }
        }
        this.all_data.push({offer_id:campg_id,amount:price});
      
        this.all_data = this.all_data.filter(function( element ) {
          return element !== undefined;
        });
      }else{
        this.all_data.push({offer_id:campg_id,amount:price});
      }

      this.jsondata.storage = this.all_data;
      
      this.totalamount =  +c_price + this.totalamount;      
    } else {
        this.positiveAmt = !this.positiveAmt;
        var index = this.all_data.indexOf(campg_id);
        this.all_data.splice(index,1);
        this.jsondata.storage = this.all_data;

        this.totalamount = this.totalamount - c_price;
        if(this.totalamount <= 0 ){
          this.positiveAmt = false;
          this.disabled = false;
          this.totalamount = 0;
        };      
      }
      console.log(this.all_data)

  }


}
