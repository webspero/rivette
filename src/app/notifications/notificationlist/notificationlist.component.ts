import { Component, OnInit } from '@angular/core';
import { RivetteService } from  '../../rivette.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-notificationlist',
  templateUrl: './notificationlist.component.html',
  styleUrls: ['./notificationlist.component.scss'],
})
export class NotificationlistComponent implements OnInit {
  custId;
  client_id;
  notificationsList;
  noRecords:boolean;
  constructor(
    private router: Router,
    private apiService:  RivetteService
    ) { }

  ngOnInit() {
    this.custId = localStorage.getItem('CustomerID');
    this.client_id = localStorage.getItem('client_id');
    let reqdata = {
      "auth": "usman@2019",
      "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
      "customer_id":this.custId,
      "client_id":this.client_id
    }
    this.apiService.notifications(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        this.notificationsList = res['message']["notifications"];
        console.log(res);
      }else {
        this.noRecords = true;
        console.log('notifications not found');
      }
    });
  }
  
  checkClass(status=null){
    if(status == "2"){
      return 'read-notification';
    }else{
      return 'unread-notification';
    }
  }

  spcNotificationClick(campId= null,notifId=null){
    let reqdata = {
      "auth": "usman@2019",
      "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
      "customer_id":this.custId,
      "client_id":this.client_id,
      "notification_id":notifId,
      "status":"2"
    } 
    this.apiService.updateNotifications(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        console.log(res['message']);
        this.router.navigate(['/campaign-detail/',campId]);
      }else {
        console.log('cust not found');
      }
    });   
  }


}
