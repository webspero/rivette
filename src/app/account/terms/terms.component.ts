import { Component, OnInit } from '@angular/core';
import { RivetteService } from '../../rivette.service';    
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
})
export class TermsComponent implements OnInit {
  contentType;
  termsCondition;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: RivetteService
  ) { }

  ngOnInit() {
    this.contentType = this.activatedRoute.snapshot.paramMap.get("type");
    let termsData = {
      'auth': 'usman@2019',
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };
    this.apiService.termsCondition(termsData).subscribe((res) => {
      if(res['status']=='success'){
        this.contentType = this.activatedRoute.snapshot.paramMap.get("type");
        this.termsCondition = res['message'];
        console.log(this.termsCondition);
      }
      else{
        console.log('error listing clients');
      }
    });
    
  }

}
