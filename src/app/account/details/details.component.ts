import { Component, OnInit } from '@angular/core';
import { RivetteService } from '../../rivette.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  customerdata;
  locationData;
  custId = localStorage.getItem('CustomerID');
  clientId = localStorage.getItem('client_id');
  fcmToken = localStorage.getItem('firebaseToken');
  deviceToken = localStorage.getItem('deviceToken');
  showFcm:boolean=false;

  constructor(
    private apiService: RivetteService,
    private http: HttpClient
  ) { 
    this.custId = localStorage.getItem('CustomerID');
    this.clientId = localStorage.getItem('client_id');
    
    let reqdata = {
        "auth": "usman@2019",
        "client_id": this.clientId,
        "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
        "customerID":this.custId,
        "location_info":"true"
      }
    this.apiService.searchCustomer(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        this.customerdata = res['message'].customer;
        this.locationData = res['message'].location_info[0];
      }else {
        console.log('cust not found');
        }
      });    
  }
  checkFcm(){
    alert(this.fcmToken);
  }
  checkDvc(){    
    alert(this.deviceToken);
  }
  ngOnInit() {
  }

}
