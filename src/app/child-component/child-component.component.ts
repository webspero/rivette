import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../rivette.service';
import { JsondataService } from '../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

 
@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.scss'],
})
export class ChildComponentComponent implements OnInit {
	customerdata;
    childdata;
    allChild;
    mergedData: any;
    formdata;
    phoneno;
    custadd;
    gender;
    childDiv: boolean;
    moreChild: boolean;
    is_m_user;
    showwaiver: any;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
        if (this.jsondata.hasOwnProperty('storage')) {
            this.is_m_user = this.jsondata.storage.customer.m_user;
            this.customerdata = this.jsondata.storage.customer;
            console.log(this.customerdata);
            this.childdata = this.jsondata.storage.child;
            this.gender = this.customerdata.Gender;
            this.phoneno = this.customerdata.PhoneNumber;
            if(this.customerdata.Address1){
                this.custadd = this.customerdata.Address1;
            }else{
                this.custadd = 'No Address Found';
            }
        } else {
            this.gender = 'M';
        }
    }

    ngOnInit() {
        this.formdata = new FormGroup({
            child: this.formBuilder.array([
                this.initChild(),
            ])
        });
    }

    togglediv() {
        if (this.childDiv) {
            this.childDiv = false;
        } else {
            this.childDiv = true;
        }
    }

    initChild() {
        return this.formBuilder.group({
            FirstName: [''],
            LastName: [''],
            BirthDate: [''],
            Gender: ['']
        });
    }

    addChild() {
        const control = < FormArray > this.formdata.controls['child'];
        control.push(this.initChild());
    }

    childMore() {
        if (this.moreChild) {
            this.moreChild = false;
        } else {
            this.moreChild = true;
        }
    }

    custDate(dateTime){
		if(dateTime){
			var newDate = dateTime.replace(/ /g, "T");
			let date = new Date(newDate);
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
			];
			const weekDays = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			const getDay = weekDays[date.getDay()];
			const getMonth = monthNames[date.getMonth()];
			const getDate = date.getDate();
			
			return getDay+', '+getMonth+' '+getDate;
		}
    }

    submitForm(data) {
        this.showwaiver = localStorage.getItem('showWaiver');

        for (var kk = 0; kk < data.child.length; kk++) {
            if (data.child[kk].FirstName != '') {
                data.child[kk].Gender = 'M';
                data.child[kk].CustStatusID = 1;
                data.child[kk].CustTypeID = 1;
                data.child[kk].RelationshipType = 1;
                data.child[kk].DaycareCustomerType = 1;
                data.child[kk].Handicap = 1;
                data.child[kk].CustomAddress = this.custadd;
                data.child[kk].PhoneNumber = this.phoneno;
            } else {
                var index = data.child.indexOf(kk);
                data.child.splice(index, 1);
            }
        }
        /*
        if (this.childdata) {
            this.allChild = this.childdata.concat(data.child);
        }else{
        	this.allChild = data.child;
        }
        */
        console.log(this.showwaiver);
        
        if (data.child.length > 0) {
       		data.ParentCustKey = this.customerdata.CustomerID;
        	this.mergedData = Object.assign(this.jsondata.reqData, data);
            console.log(this.mergedData);
            this.apiService.addChild(this.mergedData).subscribe((res) => {
            	console.log(res);
				if(res['status']== 'success'){
					this.router.navigate(['waiver-sign']);
            	}
            });
        } else if(this.showwaiver != 'false') {
        	this.router.navigate(['waiver-sign']);
        }else{
            this.router.navigate(['customer-jump']);
        }

    }


}
