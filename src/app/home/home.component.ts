import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastController,ActionSheetController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../rivette.service';
import { JsondataService } from "../jsondata.service";
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { CommonFuncService } from '../services/common/common-func.service';

import { Device } from '@ionic-native/device/ngx';
@Component({
	selector: 'app-home',
	templateUrl: 'home.component.html',
	styleUrls: ['home.component.scss'],
})

export class HomeComponent implements OnInit { 

	showdiv:boolean;
	formdata;
	phone;
	email;
	uniqueuid;
	custId;
  	clientId;
	
	constructor(
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private apiService:  RivetteService,
		private jsondata:  JsondataService,
		public toastController: ToastController,
		private http: HttpClient,
		private device: Device,
		private CommonFunc: CommonFuncService,
		public actionSheetController: ActionSheetController
	) {
		this.custId = localStorage.getItem('CustomerID');
    	this.clientId = localStorage.getItem('client_id');
	}
	
	ngOnInit() {
		localStorage.clear();

		this.formdata = new FormGroup({
			phone: new FormControl(null, []),
			email: new FormControl(null, []),
			uniqueuid: new FormControl(null, []),
		});

		this.formdata.controls['phone'].setValue('84561325469');
		this.formdata.controls['email'].setValue('');
		this.formdata.controls['uniqueuid'].setValue(this.uniqueuid);
	}
	
	onClickSubmit(myVal){
		console.log(myVal);
	}

	redirectSign(){
		this.router.navigate(['signup']);
	}

	getUuid(){	
		return this.device.uuid;
	}
	
	async presentToast(toastdata) {
		const toast = await this.toastController.create({
			message: toastdata,
			duration: 2000
		});
		toast.present();
	}
	async getData(data){
		let reqdata = {
			'auth': 'usman@2019',
			'customer_phone': data.phone,
			'customer_email': data.email,
			'client_id': 1,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		let httpHeaders = new HttpHeaders({
			'Content-Type' : 'application/json',
		});
		this.apiService.searchCustomer(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				//localStorage.setItem('custEmail',res['message'].customer.EMailAddress);
				
				localStorage.setItem('isValid', res['message'].customer.IsValid);
				localStorage.setItem('custData', res['message'].customer);
				localStorage.setItem('firstName', res['message'].customer.FirstName);
				localStorage.setItem('lastName', res['message'].customer.LastName);
				localStorage.setItem('CustomerID', res['message'].CustomerID);
				localStorage.setItem('client_id', res['message'].customer.client_id);
				localStorage.setItem('stripe_acc', res['message'].stripe_acc);
				localStorage.setItem('publisher_key', res['message'].publisher_key);
				localStorage.setItem('custId', res['message'].CustomerID);
            
				this.jsondata.storage = res['message'];
				this.router.navigate(['rewards-home']);
			}else{
				delete this.jsondata.storage;
				this.router.navigate(['rewards-home']);
			}
		}); 
	}

	togglediv(val){
		if(!val){
			delete this.jsondata.storage;
		}
		this.showdiv = val;
	}
	// END Togglediv

	async uploadActionSheet() {
		const actionSheet = await this.actionSheetController.create({
		  buttons: [{
			text: 'Camera',
			icon: 'camera',
			handler: () => {
			  console.log('Camera clicked');
			}
		  }, {
			text: 'Open Gallery',
			icon: 'images',
			handler: () => {
			  console.log('Gallery clicked');
			}
		  }, {
			text: 'Cancel',
			icon: 'close',
			role: 'cancel',
		  }]
		});
		await actionSheet.present();
	  }

}
