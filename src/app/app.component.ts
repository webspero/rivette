import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Firebase } from '@ionic-native/firebase/ngx';
// import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';

import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private firebase: Firebase,
    // private firebaseDynamicLinks: FirebaseDynamicLinks,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // 
      // this.firebaseDynamicLinks.onDynamicLink()
      // .subscribe((res: any) => {
      //   console.log(res)
      // }
      // ,(error:any) => {
      //   console.log(error)
      // });
      // 
      if (this.platform.is('ios')) {
        this.firebase.grantPermission()
        .then((success) => {
          if (success) {
            this.firebase.getToken()
            .then(token => {
              alert(token);
              localStorage.setItem('firebaseToken', token)
            }) 
            .catch(error => {
              console.error('Error getting token', error)
            });
          }
        });
      }else{
        this.firebase.getToken()
        .then(token => {
          alert(token);
          console.log('The token is ${token}');
          localStorage.setItem('firebaseToken', token);
        }) 
        .catch(error => {
          console.error('Error getting token', error)
        });
      }
      
      this.firebase.onNotificationOpen()
      .subscribe(data => {
        console.log('User opened a notification ${data}');
        console.log(data.landing_page);
        if (data.wasTapped) {
          console.log('Received in background');
          this.router.navigate([data.landing_page, data.camp_id]);
        } else {
          console.log('Received in foreground');
          this.router.navigate([data.landing_page, data.camp_id]);
        }
      });

      this.firebase.onTokenRefresh()
      .subscribe((token: string) => {
        localStorage.setItem('firebaseToken', token);
        console.log(token);
      });
      
    });
   
  }
}
