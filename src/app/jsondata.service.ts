import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JsondataService {
	public storage: any;
	public totalConvFee: any;
	public totalAmount: any;
	public paymentVal: any;
	public waiverChild: any;
	public purchased: any;
	public notificationCount: any;
	public reqData = {
		'auth': 'usman@2019',
		'client_id': localStorage.getItem('client_id'),
		'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe'
	};

  	constructor() { }
}
