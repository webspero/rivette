import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JsondataService } from "../../jsondata.service";
import { RivetteService } from '../../rivette.service';
// Send Parameter
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-buy-giftcard',
  templateUrl: './buy-giftcard.component.html',
  styleUrls: ['./buy-giftcard.component.scss'],
})
export class BuyGiftcardComponent implements OnInit {
	totalamount: number = 0;
	positveAmount: boolean = false;
	positiveVal: boolean = false;  
	formdata;
	manualAmount;
	selectAmount:object;
	giftId;
	giftcards;
	gift_img;
	gift_name;
  constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private apiService: RivetteService,	
		private jsondata:  JsondataService,
		public navCtrl: NavController
	) {
		this.selectAmount = ['10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','100'];
	 }

  ngOnInit() {
  		this.formdata = new FormGroup({
			manualAmount: new FormControl(null, [
				Validators.required,
				Validators.min(10),
				Validators.max(100)
			]),
			toemail: new FormControl(null, [
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]),
			tomobile: new FormControl('', []),
			from_name: new FormControl(null, [
				Validators.required,
			]),
			cusMessage: new FormControl(null, [Validators.required]),
			Quantity: new FormControl('1', []),

		});
		this.giftId = this.activatedRoute.snapshot.paramMap.get("id");
		// GET Giftcards 
		let reqdata = {
			'auth': 'usman@2019',
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'id' : this.giftId,
		  };
		  this.apiService.getGiftcards(reqdata).subscribe((res) => {
		  if(res['status']=='success'){
			  this.giftcards = res['message'];
			
			  for (let i in this.giftcards) {
				this.gift_img = this.giftcards[i][0].thumb_image;
				this.gift_name = this.giftcards[i][0].name;
				}
			}else {
				console.log('cust not found');
			}
		  });
		  // END GET Giftcards
  }
  async onSearchChange(searchValue){
	console.log(searchValue);
	// searchValue = parseInt(searchValue);
	this.positiveVal = true;
	if(searchValue > 0){
		this.positiveVal = true;
	}
  }

	removeSlash(val){
		return val.replace(/\//g,'');
	}

  async addAmount(amount){
	this.totalamount = amount; 
  }
  

  async addmanualAmount(){
	this.totalamount = 0;
	this.totalamount = this.manualAmount + this.totalamount;
  }
 
  async makePayment(data){
		this.jsondata.storage = data;
		this.router.navigate(['/giftcards/gift-payment',parseInt(data.manualAmount),this.giftId]);
}


}
