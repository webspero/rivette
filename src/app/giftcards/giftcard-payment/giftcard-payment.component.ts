import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JsondataService } from "../../jsondata.service";
import { RivetteService } from '../../rivette.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

declare var Stripe;

@Component({
  selector: 'app-giftcard-payment',
  templateUrl: './giftcard-payment.component.html',
  styleUrls: ['./giftcard-payment.component.scss'],
})
export class GiftcardPaymentComponent implements OnInit {
  	planPrice;  
	giftdata;

	qrcode;
	qrcodeImg;
	allcoupons;
	alldata;
	customerdata;

	showsuccess: boolean;
	showpayment: boolean;
	showspinner: boolean;

	useremail;
	userPhone;
	userName;
	userAddress;

	clientSecret;
	stripe;
	elements;
	cardElement;
	giftId;

	custId;
	clientId;

	ifStripe:boolean = true;
	walletBalance:number = 0;
	redeemRefill:boolean;
	amountDue;
	refillPaymentType = null;

	stripeAccount = localStorage.getItem('stripe_acc');
	publisher_key = localStorage.getItem('publisher_key');
	
  constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,			
		private apiService: RivetteService,
      	private jsondata:  JsondataService,
      	private http: HttpClient
    ) { }

  ngOnInit() {
	this.stripeAccount = localStorage.getItem('stripe_acc');
	this.publisher_key = localStorage.getItem('publisher_key');
		this.stripe = Stripe(this.publisher_key, {
			betas: ['payment_intent_beta_3'],
			stripeAccount: this.stripeAccount
		});
		this.elements = this.stripe.elements(); 
	
		this.cardElement = this.elements.create('card', {
			iconStyle: 'solid',
			style: {
				base: {
					iconColor: '#8898AA',
					color: 'black',
					lineHeight: '36px',
					fontWeight: 300,
					fontFamily: 'Helvetica Neue',
					fontSize: '19px',
					'::placeholder': {
						color: '#8898AA',
					},
				},
				invalid: {
					iconColor: '#e85746',
					color: '#e85746',
				}
			},
			classes: {
				focus: 'is-focused',
				empty: 'is-empty',
			},
		}); 

    	this.giftdata = this.jsondata.storage;
		console.log(this.giftdata);
		this.showpayment = true;
		this.showspinner = false;

		this.planPrice = this.activatedRoute.snapshot.paramMap.get("price");
		this.amountDue = this.planPrice;
		this.giftId = this.activatedRoute.snapshot.paramMap.get("id");

    
		this.setupStripe(this.planPrice); 
		// GET CUSTOMER 
		this.custId = localStorage.getItem('CustomerID');
		this.clientId = localStorage.getItem('client_id');
		
		
		let reqdata = {
			'auth': 'usman@2019',
			'customerID': this.custId,
			'client_id': this.clientId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'refill_card':true
		};
		
		this.apiService.searchCustomer(reqdata).subscribe((res) => {
		if(res['status']=='success'){
			this.customerdata = res['message'];
			this.useremail = this.customerdata.customer.EMailAddress;
			this.userPhone = this.customerdata.customer.PhoneNumber;
			this.userName = this.customerdata.customer.FirstName+' '+this.customerdata.customer.LastName;
			this.userAddress = this.customerdata.customer.Address1+' '+this.customerdata.customer.Address2;
			if (res['message'].refill_card && res['message'].refill_card.length) {
				this.walletBalance = res['message'].refill_card[0].total_amount;
			}else{

			}
		}else {
			console.log('cust not found');
			}
		});
		// END GET CUSTOMER    
	}


	refillCheck(ev: any){
		
		let ifChecked = ev.detail.checked;
		
		if(ifChecked){
			let dueAmount = (+this.planPrice) - (+this.walletBalance);
			if(dueAmount > 0){
				this.refillPaymentType = 'partial';
				dueAmount = Math.abs(dueAmount);
				console.log(typeof(dueAmount));
				if(dueAmount % 1 != 0){
					this.amountDue = dueAmount.toFixed(2);				
				}else{
					this.amountDue = Math.abs(dueAmount);				
				}
				
				this.setupStripe(this.amountDue);
				this.ifStripe = true;
			}else{
				this.refillPaymentType = 'full';
				this.amountDue = 0;
				this.ifStripe = false;
			}
			console.log(this.refillPaymentType);
		}else{
			this.refillPaymentType = '';
			this.amountDue = (+this.planPrice);
			this.setupStripe(this.amountDue);
			this.ifStripe = true;
			console.log(this.refillPaymentType);
		}
		
	}
	

  	async setupStripe(planPrice){
		var paymentIntent;
		// planPrice = planPrice/100;
	    
		this.cardElement.mount('#gift-card-element');

		let reqdata = {
			'auth': 'usman@2019',
			'stripe_account_id': this.stripeAccount,
			'amount': planPrice,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.paymentIntents(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				console.log(res);
				this.clientSecret = res['message'].client_secret;
			}else{
				console.log('error');
				console.log(res);
			}
		});

	}
	 


makePayment(clientSecret){
	this.showspinner = true;
	
	let newThis = this;

		this.stripe.handleCardPayment(clientSecret, this.cardElement, {
			source_data: {
				 owner: {
					name: this.userName,
					address: {
						line1: this.userAddress,
					},
					email: this.useremail,
					phone: this.userPhone
				}
			},
			}).then(function(result) {
				if (result.error) {
					newThis.showspinner = false;
					alert(result.error.message);
					}
					else if(result.paymentIntent && result.paymentIntent.status === 'succeeded') {
					console.log(result);
						let paymentIntentid = result.paymentIntent.id;
						localStorage.setItem('tranId',result.paymentIntent.id);
						var paymentIntentStatus = result.paymentIntent.status;
						if (paymentIntentStatus == 'succeeded') {
							newThis.showspinner = false;
							newThis.showsuccess = true;
							newThis.showpayment = false;
							let doTransacdata = newThis.doTransac(paymentIntentid);
						}

				}else {
						console.log('result');
			}
		});
	}
	//
	doTransac(paymentIntentid){
		console.log(this.planPrice);
		console.log(this.walletBalance);
		if (typeof this.planPrice != "number") {
			this.planPrice = +(this.planPrice);
		}	
		if(this.refillPaymentType == 'full' ||  this.refillPaymentType == 'partial'){
			console.log('if');
			if(this.planPrice > this.walletBalance){
				console.log('walletBal');
				var refill_amount:number = this.walletBalance;
			}else if(this.planPrice <= this.walletBalance){
				console.log('planPriceBal');
				var refill_amount:number = this.planPrice;	
			}
			var transactionData:{} ={
				"auth": "usman@2019",
				"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
				"client_id": this.clientId,
				"customer_id": this.customerdata.customer.CustomerID,
				"amount": this.planPrice,
				"transection_id": paymentIntentid,
				"order_type": "2",
				
				"to_email":this.giftdata.toemail,
				"phone_no":this.giftdata.tomobile,
				
				"from_name":this.giftdata.from_name,
				"custom_msg":this.giftdata.cusMessage,
				"gift_card_id": this.giftId,

				"refill_payment_type":this.refillPaymentType,
				"refill_amount":refill_amount,
				
				"orders" : [
					{
						"inventory_id": "0",
						"quantity": this.giftdata.Quantity,
						"amount": this.planPrice
						
					}
				]			
			};
		}else{	
			console.log('else');
			var transactionData:{} ={
				"auth": "usman@2019",
				"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
				"client_id": this.clientId,
				"customer_id": this.customerdata.customer.CustomerID,
				"amount": this.planPrice,
				"transection_id": paymentIntentid,
				"order_type": "2",
				
				"to_email":this.giftdata.toemail,
				"phone_no":this.giftdata.tomobile,
				"from_name":this.giftdata.from_name,
				"custom_msg":this.giftdata.cusMessage,
				"gift_card_id": this.giftId,
				
				"orders" : [
					{
						"inventory_id": "0",
						"quantity": this.giftdata.Quantity,
						"amount": this.planPrice
						
					}
				]			
			};
		}
		console.log(transactionData);		

		this.apiService.doTransaction(transactionData).subscribe((res) => {
			if(res['status']=='success'){
				this.showsuccess = true;
				this.showpayment = false;
				this.alldata = res['message'];
				this.qrcode = res['message'].qrcode;
				this.qrcodeImg = res['message'].qrcode_img;
				this.allcoupons = res['message'].coupons;
				console.log(res);
				return(res['message']);
			}else{
				alert('error');
			}
		});
		// End Do Transaction
	}; 

}
