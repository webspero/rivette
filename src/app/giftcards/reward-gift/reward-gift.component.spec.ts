import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardGiftComponent } from './reward-gift.component';

describe('RewardGiftComponent', () => {
  let component: RewardGiftComponent;
  let fixture: ComponentFixture<RewardGiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardGiftComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardGiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
