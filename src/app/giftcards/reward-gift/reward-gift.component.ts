import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JsondataService } from "../../jsondata.service";
import { RivetteService } from '../../rivette.service';
// Send Parameter
import { NavController } from '@ionic/angular';
import { CommonFuncService } from '../../services/common/common-func.service';
@Component({
  selector: 'app-reward-gift',
  templateUrl: './reward-gift.component.html',
  styleUrls: ['./reward-gift.component.scss'],
})
export class RewardGiftComponent implements OnInit {totalamount: number = 0;
	positveAmount: boolean = false;
	positiveVal: boolean = false;  
	formdata;
	manualAmount;
	selectAmount:object;
	giftId;
	rewardValue;
	invId;	

	giftcards;
	gift_img;
	gift_name;

	custId;
	clientId;
  
  constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private apiService: RivetteService,	
		private jsondata:  JsondataService,
		public navCtrl: NavController,
		private CommonFunc: CommonFuncService
	) {
		this.selectAmount = ['10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','100'];
	 }

  ngOnInit() {
    this.giftId = this.activatedRoute.snapshot.paramMap.get("id");
    this.rewardValue = this.activatedRoute.snapshot.paramMap.get("val");
	this.manualAmount = this.activatedRoute.snapshot.paramMap.get("price");
	this.invId = this.activatedRoute.snapshot.paramMap.get("inv");
	
    // GET CUSTOMER 
		this.custId = localStorage.getItem('CustomerID');
    this.clientId = localStorage.getItem('client_id');
    
    this.formdata = new FormGroup({
			manualAmount: new FormControl(this.manualAmount, []),
			toemail: new FormControl(null, [
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]),
			tomobile: new FormControl('', []),
			from_name: new FormControl(null, [
				Validators.required,
			]),
			cusMessage: new FormControl(null, [Validators.required]),
			Quantity: new FormControl('1', []),

    });
    
    
		// GET Giftcards 
		let reqdata = {
			'auth': 'usman@2019',
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'id' : this.giftId,
		  };
		  this.apiService.getGiftcards(reqdata).subscribe((res) => {
		  if(res['status']=='success'){
			  this.giftcards = res['message'];			
			  for (let i in this.giftcards) {
  				this.gift_img = this.giftcards[i][0].thumb_image;
  				this.gift_name = this.giftcards[i][0].name;
				}
			}else {
				console.log('cust not found');
			}
		  });
		  // END GET Giftcards
  }

  

	removeSlash(val){
		return val.replace(/\//g,'');
	}

 
  
 
  async makePayment(data){
    
    let transactionData = {
			"auth": "usman@2019",
			"authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
			"order_type": "2",
			"redeem_reward_points":"true",

			"client_id": this.clientId,
			"customer_id": this.custId,
			"amount": this.manualAmount,

			"to_email":data.toemail,
			"from_name":data.from_name,
			"custom_msg":data.cusMessage,
			"gift_card_id": this.giftId,
			"point_redeemed":this.rewardValue,
			"orders" : [
				{
					"inventory_id": this.invId,
					"quantity": data.Quantity,
					"amount": this.manualAmount
					
				}
			]			
		};
		
		this.apiService.doTransaction(transactionData).subscribe((res) => {
			if(res['status']=='success'){
        alert('Reward Points Redeemed Successfully');
        this.CommonFunc.showToast('Reward Points Redeemed Successfully');
				this.router.navigate(['/reward/select']);
			}else{
				alert('error');
			}
  });
}


}
