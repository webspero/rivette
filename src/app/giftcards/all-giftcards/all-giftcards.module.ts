import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllGiftcardsPage } from './all-giftcards.page';

const routes: Routes = [
  {
    path: '',
    component: AllGiftcardsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllGiftcardsPage]
})
export class AllGiftcardsPageModule {}
