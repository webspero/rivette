import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { JsondataService } from '../../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-all-giftcards',
  templateUrl: './all-giftcards.page.html',
  styleUrls: ['./all-giftcards.page.scss'],
})
export class AllGiftcardsPage implements OnInit {
	giftcards;
	featuredCards;
	constructor(
  		private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        private http: HttpClient
        ) { }

  ngOnInit() {
  	// GET Giftcards 
		let reqdata = {
	      'auth': 'usman@2019',
	      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
	    };
	    this.apiService.getGiftcards(reqdata).subscribe((res) => {
		if(res['status']=='success'){
			this.featuredCards = res['message'].Featured;
			delete(res['message'].Featured)
			this.giftcards = res['message'];
			console.log(this.giftcards);
			console.log(this.featuredCards);
		}else {
	      	console.log('cust not found');
	      }
	    });
		// END GET Giftcards
  	}

  	removeSlash(val){
  		return val.replace(/\//g,'');
	}
	
    buyGiftcard(id){
      this.router.navigate(['/giftcards/buy-gift',parseInt(id)]);
    }

}
