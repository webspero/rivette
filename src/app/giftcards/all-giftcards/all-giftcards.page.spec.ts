import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGiftcardsPage } from './all-giftcards.page';

describe('AllGiftcardsPage', () => {
  let component: AllGiftcardsPage;
  let fixture: ComponentFixture<AllGiftcardsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGiftcardsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGiftcardsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
