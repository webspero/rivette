import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TilesScreensComponent } from './tiles-screens.component';

describe('TilesScreensComponent', () => {
  let component: TilesScreensComponent;
  let fixture: ComponentFixture<TilesScreensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TilesScreensComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TilesScreensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
