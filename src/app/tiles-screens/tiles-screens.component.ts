import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../rivette.service';
import { JsondataService } from "../jsondata.service";
import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-tiles-screens',
  templateUrl: './tiles-screens.component.html',
  styleUrls: ['./tiles-screens.component.scss'],
})
export class TilesScreensComponent implements OnInit {

	customerdata;
    childdata;
    allChild;
    mergedData: any;
    formdata;
    gender;
    childDiv: boolean;
    moreChild: boolean;
    udpate: boolean;
    m_user: boolean;
    show_waiver: any;
    is_m_user;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient,
		private device: Device
    ) {
        // localStorage.removeItem("custId");
        // // localStorage.removeItem("custPhone");
        // localStorage.removeItem("isValid");
        // localStorage.removeItem("firstName");
        // localStorage.removeItem("lastName");
        if (this.jsondata.hasOwnProperty('storage')) {
            this.is_m_user = this.jsondata.storage.customer.m_user;
            this.customerdata = this.jsondata.storage.customer;
            this.childdata = this.jsondata.storage.child;
            this.udpate = true;
            this.gender = this.customerdata.Gender;
            localStorage.setItem('isValid', this.customerdata.IsValid);
            localStorage.setItem('firstName', this.customerdata.FirstName);
            localStorage.setItem('lastName', this.customerdata.LastName);
			localStorage.setItem('custPhone', this.customerdata.PhoneNumber);
			localStorage.setItem('custId', this.customerdata.CustomerID);
            if (this.is_m_user == "1") {
                this.m_user = true;
            } else {
                this.m_user = false;
            }
        } else {
        	this.gender = 'M';
            this.udpate = false;
            this.m_user = false;
        }
    }

  ngOnInit() {
      
  }

}
