import { Component, OnInit } from '@angular/core';
import { RivetteService } from '../../rivette.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-redeemed',
  templateUrl: './redeemed.component.html',
  styleUrls: ['./redeemed.component.scss'],
})
export class RedeemedComponent implements OnInit {
  custId;
  client_id;
  orders;
  noRecords:boolean=false;
  constructor(
    private apiService: RivetteService,
    private http: HttpClient,
  ) {
    this.custId = localStorage.getItem('CustomerID');
    this.client_id = localStorage.getItem('client_id');
    console.log(this.custId);
   }

  ngOnInit() {
    let reqData = {
      "auth":"usman@2019",
      "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
      "customer_id":this.custId,      
      "client_id":this.client_id,
      "start_limit":"0",
      "end_limit":"20"
    }
    this.apiService.rewardsPointSummary(reqData).subscribe((res) => {
			if(res['status']== 'success'){
        this.orders = res['message']['order_info'];
        console.log(this.orders);
			}else{
				this.noRecords = true;
			}			
		});

  }

}
