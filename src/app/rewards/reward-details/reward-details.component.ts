import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reward-details',
  templateUrl: './reward-details.component.html',
  styleUrls: ['./reward-details.component.scss'],
})
export class RewardDetailsComponent implements OnInit {
  contentType;

  constructor(
    private activatedRoute: ActivatedRoute	
  ) { }

  ngOnInit() {
    this.contentType = this.activatedRoute.snapshot.paramMap.get("type");
  }

}
