import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute} from '@angular/router';
import { RivetteService } from  '../../rivette.service';
import { JsondataService } from "../../jsondata.service";
import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Device } from '@ionic-native/device/ngx';


@Component({
  selector: 'app-rewards-home',
  templateUrl: './rewards-home.component.html',
  styleUrls: ['./rewards-home.component.scss'],
})
export class RewardsHomeComponent implements OnInit {
    customerdata;
    childdata;
    allChild;
    mergedData: any;
    formdata;
    gender;
    childDiv: boolean;
    moreChild: boolean;
    udpate: boolean;
    m_user: boolean;
    show_waiver: any;
    is_m_user;
    featuredCards;
    campaigns;
    singleValue4;
    custName;

    custId;
    client_id;

    maxDiscount:number=10;
    
    classId:number=1;
    activeReward:number=1;
    rewardColor= 'success';
    rewardOptions;
    rewardPoints;
    currentRewardPoints:number=0;
    currentPercent:number=0;
    currentStage;
    notificationCount:any=0;
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiService: RivetteService,
    private jsondata: JsondataService,
    public toastController: ToastController,
    private http: HttpClient,
    private device: Device
  ) { 
      this.singleValue4 = 200;
    
        if (this.jsondata.hasOwnProperty('storage')) {
          if(this.jsondata.storage.customer){
            this.is_m_user = this.jsondata.storage.customer.m_user;
            this.customerdata = this.jsondata.storage.customer;
            this.childdata = this.jsondata.storage.child;
            this.udpate = true;
            this.gender = this.customerdata.Gender;
            localStorage.setItem('isValid', this.customerdata.IsValid);
            localStorage.setItem('firstName', this.customerdata.FirstName);
            localStorage.setItem('lastName', this.customerdata.LastName);
            localStorage.setItem('custPhone', this.customerdata.PhoneNumber);
            localStorage.setItem('CustomerID', this.customerdata.CustomerID);
            localStorage.setItem('client_id', this.customerdata.client_id);
        
            localStorage.setItem('stripe_acc_home', this.jsondata.storage.stripe_acc);
            localStorage.setItem('publisher_key_home', this.jsondata.storage.publisher_key);
        
            localStorage.setItem('custId', this.customerdata.CustomerID);
            if (this.is_m_user == "1") {
                this.m_user = true;
            } else {
                this.m_user = false;
            }
          }
        } else {
            this.gender = 'M';
            this.udpate = false;
            this.m_user = false;
        }
  }

  slidesOpts = {
    slidesPerView: 'auto',
    spaceBetween: 10,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }, 
  }

  checkClass(currentStage=null,indx=null){
    if(currentStage>indx){
      return 'makeGreen';
    }else{
      return 'makeOrange';      
    }
  }

  ngOnInit() {
    this.custId = localStorage.getItem('CustomerID');
    this.client_id = localStorage.getItem('client_id');
    let reqdata = {
      'auth': 'usman@2019',
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      'category':'Featured'
    };
    this.apiService.getGiftcards(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        this.featuredCards = res['message'].Featured;
      }else {
          console.log('cust not found');
        }
      });

      let campdata = {
        'auth': 'usman@2019',
        'client_id': this.client_id,
        'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      };
      this.apiService.getCampaigns(campdata).subscribe((res) => {
        this.campaigns = res['message'];
        for (let key in this.campaigns) {
          let percentVal:number = this.campaigns[key].max_percent;
          if(typeof(percentVal) !== 'number'){
            percentVal = +percentVal;
          }
          
          if(this.maxDiscount < percentVal){
            this.maxDiscount = percentVal;
          }          
        }
      });

      
  }
  
  ionViewWillEnter() {
    this.custName = localStorage.getItem('firstName');
    let rewardPoints = {
      'auth': 'usman@2019',
      'client_id': this.client_id,
      'customer_id':this.custId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      'refill_card':'true',
      'reward_points':'true',
      'membership_status':'true'
    };
    this.apiService.customerDetails(rewardPoints).subscribe((res) => {
      if(res['status']=='success'){                
        this.rewardPoints = res['message'].reward_points[0];
        if(this.rewardPoints){
          this.currentRewardPoints = this.rewardPoints.current_reward_points;
        }

        this.notificationCount = res['message'].notification;
        if(this.notificationCount == '0'){
        }else{
          this.jsondata.notificationCount = this.notificationCount;
          localStorage.setItem('notificationCount', this.notificationCount);
        }
        // currentRange
        var rewardThresholds=[];
        console.log(typeof(this.rewardOptions));       
        console.log(this.rewardOptions);        
        if (!this.rewardOptions) {
          setTimeout(() => {
            this.rewardOptions.forEach(function (value) {
              rewardThresholds.push(value.rewardValue);
            });                        
          }, 1500); 
        }else{
          if(typeof(this.rewardOptions) == 'object'){
          this.rewardOptions.forEach(function (value) {
            rewardThresholds.push(value.rewardValue);
          });
          }else{
            console.log('noData');
          }
        }
        
        
        var self = this;
        rewardThresholds.forEach(function (rewardvalue, index) {
          let currRewardPoints = +(self.currentRewardPoints);
          
          rewardvalue = +(rewardvalue);
          
          if(currRewardPoints){            
            if(currRewardPoints >= rewardvalue){              
              self.currentStage = index+1;              
              self.classId = self.currentStage;
              self.activeReward= self.currentStage;
            }
          }else{
            console.log('undefined');           
          }          
        });
        // End currentRange
      }else{
        console.log('Customer details not found');
      }
      
    });
    this.getRewardRules();
  }

  getRewardRules(){
    let rewarData = {
      'auth': 'usman@2019',
      'client_id': this.client_id,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };
    this.apiService.getRewardRules(rewarData).subscribe((res) => {
      this.rewardOptions = res['message'];
      let maxReward = this.rewardOptions[4].rewardValue;
      if(maxReward){
        if(typeof(maxReward) != 'number'){
          maxReward = +(maxReward);
        }
        if (!this.currentRewardPoints || this.currentRewardPoints != 0) {
          setTimeout(() => {
            this.currentPercent = this.currentRewardPoints/parseInt(maxReward)*100;
                        
          }, 2500); 
        }
        
        
      }
      
    });
  }

  buyGiftcard(id){
    this.router.navigate(['/giftcards/buy-gift',parseInt(id)]);
  }

  currentRange(){
    this.currentRewardPoints;
    var rewardThresholds:[];
    this.rewardOptions.forEach(function (value) {
      console.log(value);
    });
  }

  addNewClass(rewValue){
    console.log(rewValue);
    if(rewValue == 25){
      return 'box_5';
    }
    if(rewValue == 75){
      return 'box_4';
    }
    if(rewValue == 150){
      return 'box_3';
    }
    if(rewValue == 200){
      return 'box_2';
    }
    if(rewValue == 400){
      return;
    }
    
  }

  rewardPage(){
    
  }

  addClass(id){
  }
  isString(val) { 
    return typeof val === 'string'; 
  }

}
