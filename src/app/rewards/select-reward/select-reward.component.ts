import { Component, OnInit } from '@angular/core';
import { RivetteService } from  '../../rivette.service';
import { Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-select-reward',
  templateUrl: './select-reward.component.html',
  styleUrls: ['./select-reward.component.scss'],
})
export class SelectRewardComponent implements OnInit {
  selectedOption;
  rewardOptions;
  selectedReward:any;
  rewardPoints:number=0;
  custId;
  client_id;  
  activeSeg = 'redeem';  
  orders;
  noRecords:boolean=false;
  currentRewardPoints;
  constructor(
    private apiService: RivetteService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { 
    this.custId = localStorage.getItem('CustomerID');
    this.client_id = localStorage.getItem('client_id');
  }

  ngOnInit() {
    let rewarData = {
      'auth': 'usman@2019',
      'client_id': this.client_id,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
    };
    this.apiService.getRewardRules(rewarData).subscribe((res) => {
      this.rewardOptions = res['message'];
    });
    
  }
  
  ionViewWillEnter() {
    this.getDetails();
    this.getRewardSummary();  
    this.activeSeg = 'redeem';
  }

  getRewardSummary(){
    let reqData = {
      "auth":"usman@2019",
      "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
      "customer_id":this.custId,      
      "client_id":this.client_id,
      "start_limit":"0",
      "end_limit":"20"
    }
    this.apiService.rewardsPointSummary(reqData).subscribe((res) => {
			if(res['status']== 'success'){
        this.orders = res['message']['order_info'];
			}else{
				this.noRecords = true;
			}			
		});
  }
  upadatePoints(activeSeg){
    if(activeSeg=='redeemed'){
      this.rewardPoints = null;
      console.log('asdf');
      this.getDetails();
    }else{
      console.log('sdfsd');
    }

  }
  getDetails(){
    let rewardPoints = {
      'auth': 'usman@2019',
      'client_id': this.client_id,
      'customer_id':this.custId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      'refill_card':'true',
      'reward_points':'true',
      'membership_status':'true'
    };  
    this.apiService.customerDetails(rewardPoints).subscribe((res) => {
      console.log(res);
      if(res['status']=='success'){
        console.log(res);

        var getPoints = res['message'].reward_points[0];
        this.rewardPoints = +(getPoints.current_reward_points);
        var rewardThresholds=[];        
        
        this.rewardOptions.forEach(function (value) {
          rewardThresholds.push(value.rewardValue);
        });        
      }else{
        console.log('Customer details not found');
      }
      
    });
  }
  redeemReward(){
    if(this.selectedReward){
      this.selectedOption = this.selectedReward;
      let tempArray = this.selectedOption.split(",");
      let invId = parseInt(tempArray[0]);
      let rewardVal = parseInt(tempArray[1]);
     
      let rewardPrice = tempArray[2];
      
      if(tempArray[3]==2){
        let giftId:number = 25;
        if(tempArray[4]){
          giftId = tempArray[4];
        }
        this.router.navigate(['giftcards/reward-gift',giftId,tempArray[1],tempArray[2],parseInt(tempArray[0])]);
      }else{
        let reqData = {
          "auth":"usman@2019",
          "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
          "client_id":this.client_id,
          "customer_id":this.custId,
          "amount":rewardPrice,
          "order_type":"1",
          "redeem_reward_points":"true",
          "point_redeemed":rewardVal,
          "orders":[
            {
              "inventory_id":invId,
              "quantity":"1",
              "amount":rewardPrice
            }
          ]
        }
        console.log(reqData);
        this.apiService.doTransactionReward(reqData).subscribe((res) => {
          if(res['status']=='success'){
            console.log(res);
            alert("Reward points redeemed successfully");
            
            this.getRewardSummary();
            this.activeSeg = 'redeemed';
            // this.router.navigate(['reward/redeemed']);
          }else{
            console.log(res);
            alert("Can't redeem API error");
          }
        });        

      }
      
    }else{
      this.selectedReward = false;
    }    
  }

}