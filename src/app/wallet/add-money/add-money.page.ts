import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { CommonFuncService } from '../../services/common/common-func.service';
@Component({
  selector: 'app-add-money',
  templateUrl: './add-money.page.html',
  styleUrls: ['./add-money.page.scss'],
})
export class AddMoneyPage implements OnInit {
	formdata;
	totalamount:number;
	maxAmountEr;
	CustomerID;
	clientId;
	walletBalance:number = 0;
	canAddUpto:number;
	showAddAmount:boolean;
	walletOverflow:boolean;	
	maxWalletEr:boolean;
	walletTotalafter:number;
	activeSeg:any='cardMoney';
	couponCode:any;
	constructor(
		private router: Router,
		private apiService: RivetteService,
		private CommonFunc: CommonFuncService
	) {	

		
   }

   ngOnInit() {
	}

  ionViewWillEnter() {
	this.CustomerID = localStorage.getItem('CustomerID');
	this.clientId = localStorage.getItem('client_id');
  	let reqdata = {
			'auth': 'usman@2019',
			'client_id': this.clientId,
			'customerID': this.CustomerID,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'refill_card':true
		};
	this.apiService.searchCustomer(reqdata).subscribe((res) => {
		if(res['status'] == 'success'){
			if (res['message'].refill_card && res['message'].refill_card.length) {
				this.walletBalance = res['message'].refill_card[0].total_amount;
				if(this.walletBalance) {
					this.walletBalance = res['message'].refill_card[0].total_amount;
					
					this.canAddUpto = 500 - this.walletBalance;
					if (this.walletBalance == 500) {
						this.walletOverflow = true;
					}
					if (this.walletBalance <= 500) {
						this.showAddAmount = true;
					}else{
						this.showAddAmount = false;
					}
				}else{					
					this.showAddAmount = false;
				}
			} else {
				this.showAddAmount = true;
			}
						
		}else{
			this.showAddAmount = true;
			console.log('Something Went Wrong SearchCust API!!');
		}
	});
  }

 
  async addAmount(amount){
	this.maxAmountEr = false;
	this.maxWalletEr = false;
	if (isNaN(this.totalamount)){
		this.totalamount = 0;
	}

	this.totalamount = this.totalamount + amount;
	
	this.walletTotalafter = (+this.totalamount) + (+this.walletBalance);
	if (this.walletTotalafter > 500){
		this.totalamount = this.canAddUpto;
		this.maxWalletEr = true;
		return;
	}
	
	if (this.totalamount > 500){
		this.totalamount = 500;
		this.maxAmountEr = true;
		return;
	}	
  }
 
  async onInputTime(amount){
	this.maxAmountEr = false;
	this.maxWalletEr = false;
	let inputAmount = +amount;
	console.log('inputAmount'+inputAmount+'Type'+typeof(inputAmount));
	console.log('totalamount'+this.totalamount+'totalamount'+typeof(this.totalamount));
	this.walletBalance = +this.walletBalance;
	console.log('walletBalance'+this.walletBalance+'walletBalance'+typeof(this.walletBalance));
	console.log('walletTotalafter'+this.walletTotalafter+'walletTotalafter'+typeof(this.walletTotalafter));
	console.log('canAddUpto'+this.canAddUpto+'canAddUpto'+typeof(this.canAddUpto));
	
	this.walletTotalafter = inputAmount + this.walletBalance;
	if (this.walletTotalafter > 500){
		this.totalamount = this.canAddUpto;
		this.maxWalletEr = true;
		return;
	}
	if (inputAmount >= 501){
		this.totalamount = 500;
		this.maxAmountEr = true;
		return;
	}
  }

  submitCoupon(){
	let reqdata = {
			'auth': 'usman@2019',
			'customer_id': this.CustomerID,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'gift_card_code':this.couponCode,
			'client_id': this.clientId
		};
	this.apiService.redeemGiftCoupon(reqdata).subscribe((res) => {
		console.log(res);
		if(res['status'] == 'success'){
			this.walletBalance = res['message'].refill_card[0].total_amount;
			console.log(this.walletBalance);
			this.CommonFunc.showToast('Amount added successfully');
		}else{			
			// this.CommonFunc.showToast(res['message']);
			alert(res['message'].message);
		}
	});
  }


  async makePayment(data){	  
	if(data > 501){
		data = 500;
	}
	this.router.navigate(['/wallet/refill-payment',parseInt(data)]);
  }
}
