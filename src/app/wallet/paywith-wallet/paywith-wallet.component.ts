import { Component, OnInit } from '@angular/core';
import { CommonFuncService } from '../../services/common/common-func.service';
import { RivetteService } from '../../rivette.service';
@Component({
  selector: 'app-paywith-wallet',
  templateUrl: './paywith-wallet.component.html',
  styleUrls: ['./paywith-wallet.component.scss'],
})
export class PaywithWalletComponent implements OnInit {
  walletBalance:number = 0;
  custId;
  clientId;
  walletQrImg;
  walletBal:number=0;
  constructor(
    private CommonFunc: CommonFuncService,
    private apiService: RivetteService
  ) { }

  ngOnInit() {
    this.CommonFunc.sampleFnc();
    this.CommonFunc.getCustomer();
    this.custId = localStorage.getItem('custId');
	  this.clientId = localStorage.getItem('client_id');
    
  }

  ionViewWillEnter() {
    let getData = {
      'auth': 'usman@2019',
      'client_id': this.clientId,
      'customer_id':this.custId,
      'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      'refill_card':'true',
      'reward_points':'true',
      'membership_status':'true'
    };
    this.apiService.customerDetails(getData).subscribe((res) => {
      if(res['status']=='success'){
                
        var refillDetails = res['message'].refill_card[0];        
        this.walletQrImg = refillDetails.qrcode_img;
        this.walletBal = refillDetails.total_amount;
        
        
      }else{
        console.log('Customer details not found');
      }
      
    });
  }


}
