import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../../rivette.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Platform } from '@ionic/angular';

declare var Stripe;

@Component({
  selector: 'app-refill-payment',
  templateUrl: './refill-payment.component.html',
  styleUrls: ['./refill-payment.component.scss'],
})
export class RefillPaymentComponent implements OnInit {
  addAmount;  
	giftdata;

	totalAmount;
  customerdata;
  
  useremail;
  userPhone;
  userName;
  userAddress;

	showsuccess: boolean;
	showpayment: boolean;
	showspinner: boolean;

	clientSecret;
	stripe;
	elements;
	cardElement;
  giftId;


  custId = localStorage.getItem('CustomerID');
  clientId = localStorage.getItem('client_id');
  stripeAccount = localStorage.getItem('stripe_acc');
	publisher_key = localStorage.getItem('publisher_key');
  constructor(
    private platform: Platform,
    private router: Router,
    private activatedRoute: ActivatedRoute,	
    private apiService: RivetteService,
    private http: HttpClient
  ) {
  }

  ngOnInit() {

    this.custId = localStorage.getItem('CustomerID');
    this.clientId = localStorage.getItem('client_id');
    this.stripeAccount = localStorage.getItem('stripe_acc');
		this.publisher_key = localStorage.getItem('publisher_key');
    this.stripe = Stripe(this.publisher_key, {
			betas: ['payment_intent_beta_3'],
			stripeAccount: this.stripeAccount
		});
		this.elements = this.stripe.elements(); 
	
		this.cardElement = this.elements.create('card', {
			iconStyle: 'solid',
			style: {
				base: {
					iconColor: '#8898AA',
					color: 'black',
					lineHeight: '36px',
					fontWeight: 300,
					fontFamily: 'Helvetica Neue',
					fontSize: '19px',
					'::placeholder': {
						color: '#8898AA',
					},
				},
				invalid: {
					iconColor: '#e85746',
					color: '#e85746',
				}
			},
			classes: {
				focus: 'is-focused',
				empty: 'is-empty',
			},
		}); 
		
		this.showpayment = true;
		this.showspinner = false;

  this.addAmount = this.activatedRoute.snapshot.paramMap.get("amount");
  console.log(this.addAmount);
    
		this.setupStripe(this.addAmount); 
		// GET CUSTOMER 
		let reqdata = {
			'auth': 'usman@2019',
			'customerID': this.custId,
			'client_id': this.clientId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};
		this.apiService.searchCustomer(reqdata).subscribe((res) => {
		if(res['status']=='success'){
      this.customerdata = res['message'];
      this.useremail = this.customerdata.customer.EMailAddress;
			this.userPhone = this.customerdata.customer.PhoneNumber;
      this.userName = this.customerdata.customer.FirstName+' '+this.customerdata.customer.LastName;
      this.userAddress = this.customerdata.customer.Address1+' '+this.customerdata.customer.Address2;
			}else {
				console.log('cust not found');
			}
		});
	// END GET CUSTOMER
  }


  async setupStripe(addAmount){
		var paymentIntent;
		// addAmount = addAmount/100;
	    
		this.cardElement.mount('#wallet-card-element');

		let reqdata = {
			'auth': 'usman@2019',
			'stripe_account_id': this.stripeAccount,
			'amount': addAmount,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
		};

		this.apiService.paymentIntents(reqdata).subscribe((res) => {
			if(res['status']=='success'){
				console.log(res);
				this.clientSecret = res['message'].client_secret;
			}else{
				console.log('error');
				console.log(res);
			}
			console.log();
		});

	}
  // 
  
  makePayment(clientSecret){
    this.showspinner = true;
    
    let newThis = this;
  
      this.stripe.handleCardPayment(clientSecret, this.cardElement, {
        source_data: {
           owner: {
            name: this.userName,
            address: {
              line1: this.userAddress
            },
            email: this.useremail,
            phone:this.userPhone
          }
        },
        }).then(function(result) {
          if (result.error) {
            newThis.showspinner = false;
            alert(result.error.message);
            }
            else if(result.paymentIntent && result.paymentIntent.status === 'succeeded') {
              let paymentIntentid = result.paymentIntent.id;
              localStorage.setItem('tranId',result.paymentIntent.id);
              var paymentIntentStatus = result.paymentIntent.status;
              if (paymentIntentStatus == 'succeeded') {
                newThis.showspinner = false;
                newThis.showsuccess = true;
                newThis.showpayment = false;
                let doTransacdata = newThis.doTransac(paymentIntentid);
              }
  
          }else {
             alert('Error occured handleCard');
          }
      });
    }
    //
    doTransac(paymentIntentid){
      // Do Transaction
      console.log(this.customerdata.customer.CustomerID);
      let requestData = {
          "auth":"usman@2019",
          "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
          "client_id":this.clientId,
          "customer_id": this.customerdata.customer.CustomerID,
          "amount": this.addAmount,
          "paybywallet" : true,
          "transaction_id": paymentIntentid
        }
      
      this.apiService.addRefillCard(requestData).subscribe((res) => {
        if(res['status']=='success'){
          this.totalAmount = res['message'][0].total_amount;
          this.showsuccess = true;
          this.showpayment = false;
          return(res['message']);
        }else{
          alert('Error unexpected API response');
        }
      });
      // End Do Transaction
    }; 

}
