import { NgModule } from '@angular/core';

import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';

import { AuthGuard } from './auth.guard';

import { TilesScreensComponent } from './tiles-screens/tiles-screens.component';

import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { WaiverSignComponent } from './waiver-sign/waiver-sign.component';
import { CustomerJumpComponent } from './customer-jump/customer-jump.component';
import { PurchaseJumpComponent } from './purchase-jump/purchase-jump.component';
import { ChildComponentComponent } from './child-component/child-component.component';

// membership
import { SelectPrepassComponent } from './membership/select-prepass/select-prepass.component';
import { MembersInfoComponent } from './membership/members-info/members-info.component';
import { SelectMembershipComponent } from './membership/select-membership/select-membership.component';
import { SelectedMembershipComponent } from './membership/selected-membership/selected-membership.component';
import { MembershipPaymentComponent } from './membership/membership-payment/membership-payment.component';
import { MembershipDetailComponent } from './membership/membership-detail/membership-detail.component';

// Campaign
import { CampaignBuyComponent } from './campaigns/campaign-buy/campaign-buy.component';
import { CampaignDetailComponent } from './campaigns/campaign-detail/campaign-detail.component';
import { CampaignPaymentComponent } from './campaigns/campaign-payment/campaign-payment.component';

//Giftcards 
import { BuyGiftcardComponent } from './giftcards/buy-giftcard/buy-giftcard.component';
import { GiftcardPaymentComponent } from './giftcards/giftcard-payment/giftcard-payment.component';
import { RewardGiftComponent } from './giftcards/reward-gift/reward-gift.component';


//Order History 
import { OrderListComponent } from './order-history/order-list/order-list.component';

// Wallet 
import { RefillPaymentComponent } from './wallet/refill-payment/refill-payment.component';
import { PaywithWalletComponent } from './wallet/paywith-wallet/paywith-wallet.component';

//Rewards
import { RewardsHomeComponent } from './rewards/rewards-home/rewards-home.component';
import { RewardDetailsComponent } from './rewards/reward-details/reward-details.component';
import { SelectRewardComponent } from './rewards/select-reward/select-reward.component';
import { RedeemedComponent } from './rewards/redeemed/redeemed.component';

// Account
import { DetailsComponent } from './account/details/details.component';
import { TermsComponent } from './account/terms/terms.component';


// Notificationlists
import { NotificationlistComponent } from './notifications/notificationlist/notificationlist.component';


const routes: Routes = [
	{ path: '', redirectTo: 'home-screen', pathMatch: 'full' },
	// { path: '', redirectTo: 'rewards-home', pathMatch: 'full' },
	// { path: '', redirectTo: 'home', pathMatch: 'full' },
	{ path: 'home-screen', loadChildren: './home-screen/home-screen.module#HomeScreenPageModule' },
	{ path: 'home', component: HomeComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'tiles-screens', component: TilesScreensComponent,canActivate: [AuthGuard]},
	
	{ path: 'customer-info', component: CustomerInfoComponent,canActivate: [AuthGuard]},
	{ path: 'customer-child', component: ChildComponentComponent,canActivate: [AuthGuard]},
	{ path: 'waiver-sign', component: WaiverSignComponent,canActivate: [AuthGuard]},
	{ path: 'customer-jump', component: CustomerJumpComponent,canActivate: [AuthGuard]},
	{ path: 'purchase-jump', component: PurchaseJumpComponent,canActivate: [AuthGuard]},

	// Membership	
	{ path: 'membership/select-prepass', component: SelectPrepassComponent,canActivate: [AuthGuard]},	
	{ path: 'membership/members-info', component: MembersInfoComponent,canActivate: [AuthGuard]},	
	{ path: 'membership/select-membership', component: SelectMembershipComponent,canActivate: [AuthGuard]},	
	{ path: 'membership/selected-membership', component: SelectedMembershipComponent,canActivate: [AuthGuard]},
	{ path: 'membership/membership-payment/:price', component: MembershipPaymentComponent,canActivate: [AuthGuard]},
	{ path: 'membership/membership-detail', component: MembershipDetailComponent,canActivate: [AuthGuard]},

	// Campaign
	{ path: 'campaign-buy', component: CampaignBuyComponent,canActivate: [AuthGuard]},
	{ path: 'campaign-detail/:id', component: CampaignDetailComponent,canActivate: [AuthGuard]},
	{ path: 'campaign-payment/:price', component: CampaignPaymentComponent,canActivate: [AuthGuard]},
	{ path: 'campaign-payment/:price/:id', component: CampaignPaymentComponent,canActivate: [AuthGuard]},

	//Giftcards 
	{ path: 'all-giftcards', loadChildren: './giftcards/all-giftcards/all-giftcards.module#AllGiftcardsPageModule',canActivate: [AuthGuard]},
	{ path: 'giftcards/buy-gift/:id', component: BuyGiftcardComponent,canActivate: [AuthGuard]},
	{ path: 'giftcards/reward-gift/:id/:val/:price/:inv', component: RewardGiftComponent,canActivate: [AuthGuard]},
	
	{ path: 'giftcards/buy-gift', component: BuyGiftcardComponent,canActivate: [AuthGuard]},
	{ path: 'giftcards/gift-payment', component: GiftcardPaymentComponent,canActivate: [AuthGuard]},

	{ path: 'giftcards/gift-payment/:price', component: GiftcardPaymentComponent,canActivate: [AuthGuard]},
	{ path: 'giftcards/gift-payment/:price/:id', component: GiftcardPaymentComponent,canActivate: [AuthGuard]},
  	{ path: 'membership-overview/:custid', loadChildren: './membership/membership-overview/membership-overview.module#MembershipOverviewPageModule',canActivate: [AuthGuard]},
  	{ path: 'membership-overview', loadChildren: './membership/membership-overview/membership-overview.module#MembershipOverviewPageModule',canActivate: [AuthGuard]},
  	{ path: 'jump-payment/:price', loadChildren: './purchase-jump/jump-payment/jump-payment.module#JumpPaymentPageModule',canActivate: [AuthGuard]},
	{ path: 'all-giftcards', loadChildren: './giftcards/all-giftcards/all-giftcards.module#AllGiftcardsPageModule',canActivate: [AuthGuard]},
	  
  	{ path: 'add-money', loadChildren: './wallet/add-money/add-money.module#AddMoneyPageModule',canActivate: [AuthGuard]},
	{ path: 'wallet/refill-payment/:amount', component: RefillPaymentComponent,canActivate: [AuthGuard]},
	{ path: 'wallet/paywith-wallet', component: PaywithWalletComponent,canActivate: [AuthGuard]},

	{ path: 'order-history/order-list', component: OrderListComponent,canActivate: [AuthGuard]},
	
	{ path: 'rewards-home', component: RewardsHomeComponent},
	{ path: 'reward-details', component: RewardDetailsComponent,canActivate: [AuthGuard]},
	{ path: 'reward-details/:type', component: RewardDetailsComponent,canActivate: [AuthGuard]},
	{ path: 'reward/select', component: SelectRewardComponent,canActivate: [AuthGuard]},
	{ path: 'reward/redeemed', component: RedeemedComponent,canActivate: [AuthGuard] },
	
	{ path: 'account/details', component: DetailsComponent,canActivate: [AuthGuard]},
	{ path: 'account/terms', component: TermsComponent,canActivate: [AuthGuard]},
	{ path: 'account/terms/:type', component: TermsComponent,canActivate: [AuthGuard]},
	
	{ path: 'notifications/list', component: NotificationlistComponent,canActivate: [AuthGuard]},
	
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
