import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Device } from '@ionic-native/device/ngx';

import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth.guard';

// Import ng-circle-progress
import { NgCircleProgressModule } from 'ng-circle-progress';

// camera /gallery
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
// End camera/gallery

// Firebase
import { Firebase } from '@ionic-native/firebase/ngx';

import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';


// Headers
import { HeaderComponent } from './includes/header/header.component';
import { MinimalHeaderComponent } from './includes/minimal-header/minimal-header.component';
// Tabs
import { TabsComponent } from './includes/tabs/tabs.component';

// Buy Ticket
import { TilesScreensComponent } from './tiles-screens/tiles-screens.component';
//import { SearchCustomerComponent } from './search-customer/search-customer.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { ChildComponentComponent } from './child-component/child-component.component';
import { WaiverSignComponent } from './waiver-sign/waiver-sign.component';
import { CustomerJumpComponent } from './customer-jump/customer-jump.component';
import { PurchaseJumpComponent } from './purchase-jump/purchase-jump.component';

// Membership
import { SelectPrepassComponent } from './membership/select-prepass/select-prepass.component';
import { MembersInfoComponent } from './membership/members-info/members-info.component';
import { SelectMembershipComponent } from './membership/select-membership/select-membership.component';
import { SelectedMembershipComponent } from './membership/selected-membership/selected-membership.component';
import { MembershipPaymentComponent } from './membership/membership-payment/membership-payment.component';
import { MembershipDetailComponent } from './membership/membership-detail/membership-detail.component';

// Campaign
import { CampaignBuyComponent } from './campaigns/campaign-buy/campaign-buy.component';
import { CampaignDetailComponent } from './campaigns/campaign-detail/campaign-detail.component';
import { CampaignPaymentComponent } from './campaigns/campaign-payment/campaign-payment.component';

//Giftcards 
import { BuyGiftcardComponent } from './giftcards/buy-giftcard/buy-giftcard.component';
import { GiftcardPaymentComponent } from './giftcards/giftcard-payment/giftcard-payment.component';
import { RewardGiftComponent } from './giftcards/reward-gift/reward-gift.component';

//Order History 
import { OrderListComponent } from './order-history/order-list/order-list.component';

//wallet
import { RefillPaymentComponent } from './wallet/refill-payment/refill-payment.component';
import { PaywithWalletComponent } from './wallet/paywith-wallet/paywith-wallet.component';

//Rewards
import { RewardsHomeComponent } from './rewards/rewards-home/rewards-home.component';
import { RewardDetailsComponent } from './rewards/reward-details/reward-details.component';
import { SelectRewardComponent } from './rewards/select-reward/select-reward.component';
import { RedeemedComponent } from './rewards/redeemed/redeemed.component';

// Account
import { DetailsComponent } from './account/details/details.component';
import { TermsComponent } from './account/terms/terms.component';
// Notification
import { NotificationlistComponent } from './notifications/notificationlist/notificationlist.component';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		SignupComponent,
		TilesScreensComponent,
		
		CustomerInfoComponent,
		ChildComponentComponent,
		WaiverSignComponent,
		CustomerJumpComponent,
		PurchaseJumpComponent,

		SelectPrepassComponent,
		MembersInfoComponent,
		MembershipDetailComponent,
		SelectMembershipComponent,
		SelectedMembershipComponent,
		MembershipPaymentComponent,
		
		CampaignBuyComponent,
		CampaignDetailComponent,
		CampaignPaymentComponent,
		
		HeaderComponent,
		MinimalHeaderComponent,
		TabsComponent,
		
		BuyGiftcardComponent,
		GiftcardPaymentComponent,
		RewardGiftComponent,
		
		OrderListComponent,
				
		RefillPaymentComponent,
		PaywithWalletComponent,
		
		RewardsHomeComponent,
		RewardDetailsComponent,
		SelectRewardComponent,
		RedeemedComponent,
		
		DetailsComponent,
		TermsComponent,

		NotificationlistComponent
	],
	entryComponents: [],
	imports: [
		BrowserModule, 
		HttpClientModule, 
		IonicModule.forRoot(), 
		IonicStorageModule.forRoot(),
		AppRoutingModule,
		FormsModule,
		NgCircleProgressModule.forRoot({
			// set defaults here
			radius: 100,
			outerStrokeWidth: 8,
			innerStrokeWidth: 4,
			outerStrokeColor: "#e34b00",
			innerStrokeColor: "#e85e1a",
			animationDuration: 300,
			units:" / 500",
			subtitle:"Reward Points"
		}),
		ReactiveFormsModule
	],
	providers: [
		StatusBar,
		SplashScreen,
		Device,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		Camera,
	    File,
	    Firebase,
		FileTransfer,
	    WebView,
	    FilePath,
		ImagePicker,
		Crop
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
