import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { RivetteService } from '../rivette.service';
import { JsondataService } from '../jsondata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-customer-info',
    templateUrl: './customer-info.component.html',
    styleUrls: ['./customer-info.component.scss'],
})
export class CustomerInfoComponent implements OnInit {
    customerdata;
    childdata;
    allChild;
    mergedData: any;
    formdata;
    gender;
    childDiv: boolean;
    moreChild: boolean;
    udpate: boolean;
    m_user: boolean;
    show_waiver: any;
    is_m_user;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private apiService: RivetteService,
        private jsondata: JsondataService,
        public toastController: ToastController,
        private http: HttpClient
    ) {
        // localStorage.removeItem("custId");
        localStorage.removeItem("pageType");
        localStorage.removeItem("isValid");
        localStorage.removeItem("firstName");
        localStorage.removeItem("lastName");
        if (this.jsondata.hasOwnProperty('storage')) {
            this.is_m_user = this.jsondata.storage.customer.m_user;
            this.customerdata = this.jsondata.storage.customer;
            this.childdata = this.jsondata.storage.child;
            this.udpate = true;
            this.gender = this.customerdata.Gender;
            console.log(this.customerdata);
            // localStorage.setItem('custId', this.customerdata.IsValid);
            localStorage.setItem('isValid', this.customerdata.IsValid);
            localStorage.setItem('firstName', this.customerdata.FirstName);
            localStorage.setItem('lastName', this.customerdata.LastName);
            
            if (this.is_m_user == "1") {
                this.m_user = true;
            } else {
                this.m_user = false;
            }
        } else {
        	this.gender = 'M';
            this.udpate = false;
            this.m_user = false;
        }
    }

    ngOnInit() {
        this.formdata = new FormGroup({
            FirstName: new FormControl(null, Validators.required),
            LastName: new FormControl(null, Validators.required),
            EMailAddress: new FormControl(null, Validators.required),
            PhoneNumber: new FormControl(null, Validators.required),
            State: new FormControl(null, []),
            ZipCode: new FormControl(null, []),
            Address1: new FormControl(null, []),
            Gender: new FormControl(null, []),
            child: this.formBuilder.array([
                this.initChild(),
            ])
        });
        if (this.udpate || this.m_user) {
            this.formdata.controls['FirstName'].setValue(this.customerdata.FirstName);
            this.formdata.controls['LastName'].setValue(this.customerdata.LastName);
            this.formdata.controls['EMailAddress'].setValue(this.customerdata.EMailAddress);
            this.formdata.controls['PhoneNumber'].setValue(this.customerdata.PhoneNumber);
            this.formdata.controls['Address1'].setValue(this.customerdata.Address1 + this.customerdata.Address2);
            this.formdata.controls['State'].setValue(this.customerdata.State);
            this.formdata.controls['ZipCode'].setValue(this.customerdata.ZipCode);
        }
    }

    togglediv() {
        if (this.childDiv) {
            this.childDiv = false;
        } else {
            this.childDiv = true;
        }
    }

    initChild() {
        return this.formBuilder.group({
            FirstName: [''],
            LastName: [''],
            BirthDate: [''],
            Gender: ['']
        });
    }

    addChild() {
        const control = < FormArray > this.formdata.controls['child'];
        control.push(this.initChild());
    }

    childMore() {
        if (this.moreChild) {
            this.moreChild = false;
        } else {
            this.moreChild = true;
        }
    }

    submitForm(data) {
        this.show_waiver = false;
        
        data.CustomAddress = '';

        for (var kk = 0; kk < data.child.length; kk++) {
            if (data.child[kk].FirstName != '') {
                data.child[kk].Gender = 'M';
            } else {
                var index = data.child.indexOf(kk);
                data.child.splice(index, 1);
            }
        }

        if(localStorage.getItem('isValid') !='1' || localStorage.getItem('firstName') != data.FirstName || localStorage.getItem('lastName') != data.LastName){
            this.show_waiver = true;
        }

        localStorage.setItem('showWaiver', this.show_waiver);

        if (this.udpate && this.m_user) {
            data.CustomerID = this.customerdata.CustomerID;
            this.mergedData = Object.assign(this.jsondata.reqData, data);

            this.apiService.modifyCustomers(this.mergedData).subscribe((res) => {
                if(res['status']== 'success'){
                    localStorage.setItem('custPhone', res['message'].PhoneNumber);
                    localStorage.setItem('custId', res['message'].CustomerID);
                    this.router.navigate(['customer-child']);
                }else{
					console.log(res['message']);
				}
            });
        } else {
            data.CustStatusID = 1;
            data.CustTypeID = 1;
            data.RelationshipType = 1;
            data.DaycareCustomerType = 1;
            data.Handicap = 1;
            data.Gender = this.gender;
            this.mergedData = Object.assign(this.jsondata.reqData, data);

            this.apiService.addCustomers(this.mergedData).subscribe((res) => {
                if(res['status']== 'success'){
                    localStorage.setItem('custPhone', res['message'].customer.PhoneNumber);
                    localStorage.setItem('custId', res['message'].customer.CustomerID);

                    this.jsondata.storage = res['message'];
                    this.router.navigate(['customer-child']);
                }
            });
        }

    }
}