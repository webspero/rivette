import { Injectable } from '@angular/core';
import { RivetteService } from '../../rivette.service';

import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';

// import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

// import { File, FileEntry } from '@ionic-native/file/ngx';
// import { HttpClient } from '@angular/common/http';
// import { WebView } from '@ionic-native/ionic-webview/ngx';
// import { Storage } from '@ionic/storage';
// import { FilePath } from '@ionic-native/file-path/ngx';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
 
// import { JsondataService } from '../../jsondata.service';

// import { Router, ActivatedRoute } from '@angular/router';

// import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonFuncService {
  customerData;
  imageData:any='';
  image:any='';
  profilePic; 
  constructor(
    private apiService: RivetteService,
    private toastController: ToastController,
    private camera: Camera,
    private transfer: FileTransfer,
	private loadingController: LoadingController,
	// private file: File,
	// private http: HttpClient,
	// private webview: WebView,

	// private actionSheetController: ActionSheetController,

	// private storage: Storage,
	// private plt: Platform,

	// private ref: ChangeDetectorRef,
	// private filePath: FilePath,
	// private imagePicker: ImagePicker,

	// private activatedRoute: ActivatedRoute,
    // private router: Router,

    // private jsondata: JsondataService,

  ) { }
  sampleFnc(){
    console.log('ss');
  }

  openCam(custId=null){

		const options: CameraOptions = {
			quality: 70,
			allowEdit: true,
			targetWidth: 450,
			targetHeight: 450,
			destinationType: this.camera.DestinationType.FILE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		}
		
		this.camera.getPicture(options).then((imageData) => {
		// imageData is either a base64 encoded string or a file URI
		// If it's base64 (DATA_URL):
		this.imageData=imageData
		this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
		var uploadRes = this.upload(custId);
		console.log(uploadRes);
		return uploadRes;
		}, (err) => {
		 // Handle error
		 alert("error "+JSON.stringify(err))
		});
	
  }


  openGallery(custId=null){
		const options: CameraOptions = {
		  quality: 70,
		  allowEdit: true,
		  targetWidth: 450,
		  targetHeight: 450,
		  destinationType: this.camera.DestinationType.FILE_URI,
		  encodingType: this.camera.EncodingType.JPEG,
		  mediaType: this.camera.MediaType.PICTURE,
		  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
		}
		
		this.camera.getPicture(options).then((imageData) => {
			// imageData is either a base64 encoded string or a file URI
			// If it's base64 (DATA_URL):
			this.imageData=imageData
			this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
			var uploadRes = this.upload(custId);
			console.log(uploadRes);
			return uploadRes;
		}, (err) => {
			// Handle error
			alert("error "+JSON.stringify(err))
		});
	
  }
    
    async upload(custId=null)
	{	
		let client_id = localStorage.getItem('client_id'); 
		console.log(this.imageData);
		let photo_data = {
			'auth': 'usman@2019',			
			'client_id': client_id,
			'CustomerID':custId,
			'authorization':'4c1f77c8cc97a3b21dbbbff7655e29fe',
			'profile_pic' : this.imageData,
		};

		const loading = await this.loadingController.create({
		message: 'Uploading...',
		});
		await loading.present();
	
		const fileTransfer: FileTransferObject = this.transfer.create();
	
		let options1: FileUploadOptions = {
			fileKey: 'profile_pic',
			fileName: 'name.jpg',
			httpMethod: 'POST',
			headers: {
				Authorization:'4c1f77c8cc97a3b21dbbbff7655e29fe'
			},
			params: photo_data
		}
	
		fileTransfer.upload(this.imageData, 'https://app.rivette.ai/crmapi/v1.0/profile_photo.php', options1)
		.then((data) => {
			loading.dismiss();
			 
			// success
			console.log("All Data");
			console.log(data);
			console.log("data.response====>");
			console.log(data.response);

			if(typeof(data.response) == 'string'){
				var newData:any = JSON.parse(data.response);
			}else{
				var newData:any = data.response;		
			}
			console.log("data.response.message====>");
			console.log(newData.message);

			let profilePic = newData.message.profile_pic_url;

			console.log("data.response.message.profilePic====>");
			console.log(profilePic);

			if(profilePic){
				console.log('commonCtrl insideIf');
				this.profilePic = profilePic;
				console.log(profilePic);
				return this.profilePic;
			}else{
				console.log('commonCtrl ELSE');
				alert('Unusual API Response');
			}	
		}, (err) => {
			// error
			loading.dismiss();
			alert("error"+JSON.stringify(err));
		});
	}

  async showToast(message,time= 2000) {
    const toast = await this.toastController.create({
      message: message,
      duration: time
    });
    toast.present();
  }

  getCustomer(){
	let CustId = localStorage.getItem('CustomerID');
	let client_id = localStorage.getItem('client_id');  
  	let reqdata = {
			'auth': 'usman@2019',
			'client_id': client_id,
			'customerID': CustId,
			'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
			'refill_card':true
		};
	this.apiService.searchCustomer(reqdata).subscribe((res) => {
  		if(res['status'] == 'success'){
        this.customerData = res['message'];
        console.log(this.customerData);
  		}else{      
  			console.log('Something Went Wrong SearchCust API!!');
  		}
  	});
  }

  getCustomerwithId(){
    let CustID = localStorage.getItem('CustomerID');
    let ClientID = localStorage.getItem('client_id'); 

    let reqdata = {
        'auth': 'usman@2019',
        'customer_phone': CustID,
        'client_id': ClientID,
        'authorization' : '4c1f77c8cc97a3b21dbbbff7655e29fe',
      };
    this.apiService.searchCustomer(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        return res['message'];
      }else {
          console.log('cust not found');
      }
    });
    // END GET CUSTOMER
  }
}
