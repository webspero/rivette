import { TestBed } from '@angular/core/testing';

import { CommonFuncService } from './common-func.service';

describe('CommonFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonFuncService = TestBed.get(CommonFuncService);
    expect(service).toBeTruthy();
  });
});
