import { Component, OnInit } from '@angular/core';
import { JsondataService } from '../../jsondata.service';
import { RivetteService } from  '../../rivette.service';
@Component({
  selector: 'app-minimal-header',
  templateUrl: './minimal-header.component.html',
  styleUrls: ['./minimal-header.component.scss'],
})
export class MinimalHeaderComponent implements OnInit {
  custId;
  client_id;
  notificationCount;
  checkNotificationCount;
  constructor(
    private apiService:  RivetteService,
    private jsondata: JsondataService
  ) { }

  ngOnInit() {
    this.custId = localStorage.getItem('CustomerID');
    this.client_id = localStorage.getItem('client_id');
    console.log('ngOnInit');
    this.notificationCount = localStorage.getItem('notificationCount');
    if (!this.notificationCount) {
      setTimeout(() => {
        console.log(this.notificationCount);
        this.notificationCount = localStorage.getItem('notificationCount');
      }, 2500); 
    }
  }
  
  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.notificationCount = localStorage.getItem('notificationCount');
    if (!this.notificationCount) {
      setTimeout(() => {
        console.log(this.notificationCount);
        this.notificationCount = localStorage.getItem('notificationCount');
      }, 2500); 
    }
    
    // 
    let reqdata = {
      "auth": "usman@2019",
      "authorization":"4c1f77c8cc97a3b21dbbbff7655e29fe",
      "customer_id":this.custId,
      "client_id":this.client_id
    }
    this.apiService.notifications(reqdata).subscribe((res) => {
      if(res['status']=='success'){
        console.log('if');
        console.log(res);        
        this.checkNotificationCount = res['unread_notification_count'];
        console.log(this.checkNotificationCount);
      }else {
        console.log('else'+res);
        this.checkNotificationCount = 0;
        console.log('notifications not found');
      }
    });
    // 
  }

}
